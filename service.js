const OptimusAGI = require("./index");

const instance = new OptimusAGI();

instance.loadConfig().then((config) => instance.start());
