Name:		optimus-agi
Version:	1.2.4
Release:	2%{?dist}
Summary:	Optimus AGI Framework

Group:		Applications/Communications
License:	BSD
URL:		http://www.trestto.com.br
Source0:	%{name}_%{version}.tar.gz

BuildRequires:	nodejs >= 6.0.0
Requires:	nodejs >= 6.0.0, %{name}-core

%description
Pacote principal do Optimus AGI Framework

%package core
Summary:	Estrutura principal do Optimus AGI Framework
BuildArch:	noarch
Requires:	nodejs >= 6.0.0, %{name}-node-modules, asterisk-core

%package node-modules
Summary:	Dependências NodeJS para Framework Optimus AGI Framework
BuildArch:	noarch
Requires:	nodejs >= 6.0.0

%package mod-paschoalotto
Summary:	Classe base para AGI Paschoalotto
BuildArch:	noarch
Requires:	%{name}-core, %{name}-paschoalotto-ws

%package paschoalotto-ws
Summary:	Pacote de dependência WebService Paschoalotto
BuildArch:	noarch
Requires:	%{name}-core, %{name}-http, %{name}-sound

%package mod-multicobra
Summary:	Classe base para AGI Multicobra
BuildArch:	noarch
Requires:	%{name}-core, %{name}-multicobra-ws

%package multicobra-ws
Summary:	Pacote de dependência WebService Multicobra
BuildArch:	noarch
Requires:	%{name}-core, %{name}-http, %{name}-sound

%package mod-systeminteract
Summary:	Classe base para AGI System Interact
BuildArch:	noarch
Requires:	%{name}-core, %{name}-systeminteract-ws

%package systeminteract-ws
Summary:	Pacote de dependência System Interact
BuildArch:	noarch
Requires:	%{name}-core

%package mod-intervalor
Summary:	Classe base para AGI Intervalor
BuildArch:	noarch
Requires:	%{name}-core, %{name}-intervalor-ws

%package intervalor-ws
Summary:	Pacote de dependência Intervalor
BuildArch:	noarch
Requires:	%{name}-core, %{name}-http, %{name}-sound

%package sms-ip3
Summary:	Pacote de dependência SMS IP3
BuildArch:	noarch
Requires:	%{name}-core, %{name}-http

%package sms-pointer
Summary:	Pacote de dependência SMS Pointer PontalTech
BuildArch:	noarch
Requires:	%{name}-core, %{name}-http

%package sms-zenvia
Summary:	Pacote de dependência SMS Zenvia
BuildArch:	noarch
Requires:	%{name}-core, %{name}-http

%package sms-infobip
Summary:	Pacote de dependência SMS InfoBIP
BuildArch:	noarch
Requires:	%{name}-core, %{name}-http

%package sms-avant-telecom
Summary: Pacote de dependência SMS Avant Telecom
BuildArch:  noarch
Requires: %{name}-core, %{name}-http

%package sms-mind
Summary: Pacote de dependência SMS Mind
BuildArch:  noarch
Requires: %{name}-core, %{name}-http

%package sms-bestuse
Summary: Pacote de dependência SMS BestUse
BuildArch:  noarch
Requires: %{name}-core, %{name}-http

%package http
Summary:	Pacote de dependência para requests HTTP
BuildArch:	noarch
Requires:	%{name}-core, optimus-sql-agi

%package sound
Summary:	Pacote de dependência para audio da AGI
BuildArch:	noarch
Requires:	%{name}-core

%package -n optimus-sql-agi
Summary:	Pacote de arquivos SQL funcodbc para o optimus-agi
BuildArch:	noarch
Requires:	%{name}-core, optimus-sql
Requires(post):	optimus-framework-scripts >= 2.0.1


%description core
Esta pacote prove a estrutura principal do Optimus AGI Framework, contem o
serviço plug-n-play FastAGI para as URAs dos projetos Optimus

%description node-modules
Dependências NodeJS para Framework Optimus AGI Framework

%description mod-paschoalotto
Módulo para implementação da classe base das AGIs paschoalotto

%description paschoalotto-ws
Módulo de implementação do REST API para WebService Paschoalotto

%description mod-multicobra
Módulo para implementação da classe base das AGIs multicobra

%description multicobra-ws
Módulo de implementação do REST API para WebService Multicobra

%description mod-systeminteract
Módulo para implementação da classe base das AGIs System Interact

%description systeminteract-ws
Módulo de implementação de integração com discador System Interact

%description mod-intervalor
Módulo para implementação da classe base das AGIs Intervalor (QuitAqui)

%description intervalor-ws
Módulo de implementação da API QuitAqui

%description sms-ip3
Módulo de envio de SMS usando IP3

%description sms-pointer
Módulo de envio de SMS usando PontalTech

%description sms-zenvia
Módulo de envio de SMS usando Zenvia

%description sms-infobip
Módulo de envio de SMS usando InfoBIP

%description sms-avant-telecom
Módulo de envio de SMS usando Avant Telecom

%description sms-mind
Módulo de envio de SMS usando Mind

%description sms-bestuse
Módulo de envio de SMS usando BestUse

%description http
Módulo para tratamento de requisições HTTP

%description sound
Módulo para playback de audio em plano de fundo enquando a AGI está em
processamento

%description -n optimus-sql-agi
Arquivos conf funcodbc com queries para log das requisições web feitas pela
AGI

%prep
%setup -q


%build
npm install


%install
%make_install

%post core
if [ $1 -eq 1 ]; then
  /sbin/chkconfig --add optimus-agi
fi

%post -n optimus-sql-agi
if [ $1 -eq 1 ]; then
  optimus-auto-include sql add logweb 1
  asterisk -rx 'module reload func_odbc.so'
fi

%post mod-paschoalotto
asterisk -rx 'dialplan reload'

%preun core
if [ $1 -eq 0 ]; then
  /sbin/service optimus-agi stop
  /sbin/chkconfig --del optimus-agi
fi

%preun -n optimus-sql-agi
if [ $1 -eq 0 ]; then
  optimus-auto-include sql delete logweb
  asterisk -rx 'module reload func_odbc.so'
fi

%postun mod-paschoalotto
asterisk -rx 'dialplan reload'

%files core
%defattr(-, asterisk, asterisk)
%dir %{_sharedstatedir}/asterisk/agi-bin/optimus-agi
%dir %{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi
%dir %{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/index.js
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/service.sh
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/package.json
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi/sms.js
%{_sysconfdir}/rc.d/init.d/optimus-agi

%files node-modules
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/node_modules

%files mod-paschoalotto
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi/paschoalotto.js
/opt/optimus/ivr/funcoes/optimus_func_paschoalotto.conf

%files paschoalotto-ws
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/paschoalotto-ws.js

%files mod-multicobra
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi/multicobra.js

%files multicobra-ws
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/multicobra-ws.js

%files mod-systeminteract
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi/systeminteract.js

%files systeminteract-ws
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/dialer-system-interact.js

%files mod-intervalor
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi/intervalor.js

%files intervalor-ws
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/intervalor-ws.js

%files sms-ip3
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/sms-ip3.js

%files sms-pointer
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/sms-pointer.js

%files sms-zenvia
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/sms-zenvia.js

%files sms-infobip
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/sms-infobip.js

%files sms-avant-telecom
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/sms-avant-telecom.js

%files sms-mind
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/sms-mind.js

%files sms-bestuse
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/sms-bestuse.js

%files http
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/http.js

%files sound
%{_sharedstatedir}/asterisk/agi-bin/optimus-agi/agi-depends/agi-sound.js

%files -n optimus-sql-agi
/opt/optimus/ivr/sql/ivr_sql_logweb.conf

%changelog
* Wed May  2 2018 Thiago Costa <admin@rootbsd.info> 1.2.4-2
- Incluido pacote optimus-agi-sms-bestuse

* Mon Apr  9 2018 Thiago Costa <admin@rootbsd.info> 1.2.4-1
- Alteração na estrutura para envio de SMS
- Incluido pacote optimus-agi-sms-mind

* Wed Feb 21 2018 Thiago Costa <thiago.costa@trestto.com.br> 1.2.3-1
- Incluido pacote de AGI da System Interact
- Incluido pacote de AGI da Intervalor

* Wed Dec 20 2017 Thiago Costa <thiago.costa@trestto.com.br> 1.2.2-1
- Nova estrutura de diretórios /opt/optimus
- Incluido pacotes de AGI da Multicobra
- Incluido pacote optimus-agi-sms-zenvia
- Incluido pacote optimus-agi-sms-infobip

* Tue Sep 26 2017 Thiago Costa <thiago.costa@trestto.com.br> 1.2.1-1
- Nova versão 1.2.1 do core do optimus-agi (agi-sounds como dependencia do http)

* Thu Sep 21 2017 Thiago Costa <thiago.costa@trestto.com.br> 1.2.0-1
- Nova versão 1.2.0 do core do optimus-agi (incluido log4js)
- Incluido arquivo extensions_optimus.conf no pacote mod-paschoalotto

* Tue Sep 19 2017 Thiago Costa <thiago.costa@trestto.com.br> 1.1.0-1
- Nova versão 1.1.0 do core do optimus-agi

* Thu Aug 24 2017 Thiago Costa <thiago.costa@trestto.com.br> 1.0.0-2
- Incluido arquivo agi/paschoalotto.js

* Wed Aug  2 2017 Thiago Costa <thiago.costa@trestto.com.br> 1.0.0-1
- Release inicial do Framework FastAGI Optimus

