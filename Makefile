
VERSAO = 1.2.4
RPMPATH = $(shell rpm --eval '%_topdir')
RPMSOURCES = $(RPMPATH)$(if $(RPMPATH),/)SOURCES
RPMSPECS = $(RPMPATH)$(if $(RPMPATH),/)SPECS
SPECFILE = optimus-agi-core.spec

ifeq ($(shell id -u),0)
  INSTALL_CMD = install -v -p -o asterisk -g asterisk
else
  INSTALL_CMD = install -v -p
endif

BASEAGIDIR = $(DESTDIR)/var/lib/asterisk/agi-bin
AGIDIR = $(BASEAGIDIR)/optimus-agi
AGIMODDIR = $(AGIDIR)/agi
AGIDEPMODDIR = $(AGIDIR)/agi-depends
NODE_MODULES = $(AGIDIR)/node_modules
INITDIR = $(DESTDIR)/etc/rc.d/init.d

BASEOPTIMUS = $(DESTDIR)/opt/optimus
OPTIMUS_FUNCODBC = $(BASEOPTIMUS)/ivr/sql
OPTIMUS_IVRFUNC = $(BASEOPTIMUS)/ivr/funcoes

JS_FILES = index.js package.json
EXEC_FILES = service.sh
AGI_FILES = $(wildcard agi/*.js)
AGI_DEPS_FILES = $(wildcard agi-depends/*.js)
IVRFUNC_FILES = $(wildcard ivr/funcoes/*.conf)

.PHONY: targz spec install rpm

targz: $(RPMSOURCES)/optimus-agi_$(VERSAO).tar.gz
	@echo "Arquivo tar.gz: $<"

spec: $(RPMSPECS)/$(SPECFILE)
	@echo "Arquivo SPEC: $<"

rpm: targz spec
	cd "$(RPMPATH)" && rpmbuild -ba "$(patsubst $(RPMPATH)/%,%,$(RPMSPECS)/$(SPECFILE))"

install: $(foreach fn,$(JS_FILES) $(EXEC_FILES),$(AGIDIR)/$(fn)) $(foreach fn,$(notdir $(AGI_FILES)),$(AGIMODDIR)/$(fn)) $(foreach fn,$(notdir $(AGI_DEPS_FILES)),$(AGIDEPMODDIR)/$(fn)) $(NODE_MODULES) $(foreach fn,$(notdir $(IVRFUNC_FILES)),$(OPTIMUS_IVRFUNC)/$(fn)) $(INITDIR)/optimus-agi $(OPTIMUS_FUNCODBC)/ivr_sql_logweb.conf

$(RPMSOURCES)/optimus-agi_$(VERSAO).tar.gz:
	tar --exclude-vcs --exclude '.gitignore' --exclude 'dist' --transform 's/^\./optimus-agi-$(VERSAO)/' -zcvf "$@" .

$(RPMSPECS)/$(SPECFILE): dist/$(SPECFILE)
	cp -vf "$<" "$@"

$(AGIDIR):
	mkdir -p "$@"

$(AGIMODDIR): $(AGIDIR)
	mkdir -p "$@"

$(AGIDEPMODDIR): $(AGIDIR)
	mkdir -p "$@"

$(INITDIR):
	mkdir -p "$@"

$(BASEOPTIMUS):
	mkdir -p "$@"

$(OPTIMUS_FUNCODBC): $(BASEOPTIMUS)
	mkdir -p "$@"

$(OPTIMUS_IVRFUNC): $(BASEOPTIMUS)
	mkdir -p "$@"

$(AGIMODDIR)/%.js: agi/%.js $(AGIMODDIR)
	$(INSTALL_CMD) -m 0644 -T "$<" "$@"

$(AGIDEPMODDIR)/%.js: agi-depends/%.js $(AGIDEPMODDIR)
	$(INSTALL_CMD) -m 0644 -T "$<" "$@"

$(AGIDIR)/%.js: %.js $(AGIDIR)
	$(INSTALL_CMD) -m 0644 -T "$<" "$@"

$(AGIDIR)/%.json: %.json $(AGIDIR)
	$(INSTALL_CMD) -m 0644 -T "$<" "$@"

$(AGIDIR)/%.sh: %.sh $(AGIDIR)
	$(INSTALL_CMD) -m 0755 -T "$<" "$@"

$(INITDIR)/optimus-agi: init-script.sh $(INITDIR)
	$(INSTALL_CMD) -m 0644 -T "$<" "$@"

$(OPTIMUS_FUNCODBC)/ivr_sql_logweb.conf: funcodbc/ivr_sql_logweb.conf $(OPTIMUS_FUNCODBC)
	$(INSTALL_CMD) -m 0644 -T "$<" "$@"

$(OPTIMUS_IVRFUNC)/%.conf: ivr/funcoes/%.conf $(OPTIMUS_IVRFUNC)
	$(INSTALL_CMD) -m 0644 -T "$<" "$@"

$(NODE_MODULES): node_modules $(AGIDIR)
	cp -a "$<" "$@"

node_modules:
	npm install
