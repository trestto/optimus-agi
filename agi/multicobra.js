const MULTICOBRA_WS_SERVICE = 0;

module.exports = class {
  constructor() {
    this.deps = ['multicobra-ws'];
  }

  init(service, config) {
    this.service = service;
    this.config = config;
  }

  ValidarDevedor(context, cpf) {
    const paramsList = [
      {
        varName: 'HASH(DADOS_CLIENTE,CPF)',
        paramName: 'cpf',
        paramValue: cpf,
      },
    ];

    const ws = this.deps[MULTICOBRA_WS_SERVICE];

    return this._getParamsOrVars(context, paramsList)
      .then(params => ws.validarDevedor(context, params))
      .then(result => {
        if(!result['Codigo']) {
          throw new Error('Devedor inválido');
        }

        const variables = {
          'HASH(DADOS_CLIENTE,CODIGO)': result['Codigo'],
        };

        return this._setVars(context, variables);
      });
  }

  ConsultarDividasAtivas(context, cpf) {
    const paramsList = [
      {
        varName: 'HASH(DADOS_CLIENTE,CPF)',
        paramName: 'cpf',
        paramValue: cpf,
      },
    ];

    const ws = this.deps[MULTICOBRA_WS_SERVICE];

    return this._getParamsOrVars(context, paramsList)
      .then(params => ws.consultarDividasAtivas(context, params))
      .then(results => {
        if(!results.length) {
          throw new Error('Nenhuma dívida ativa');
        }

        const variables = {
          'HASH(DIVIDA,COUNT)': results.length,
        };

        results.forEach((result, index) => {
          variables[`HASH(DIVIDA,${index}_CONTRATO_NUMERO)`] = result['ContratoNumero'];
          variables[`HASH(DIVIDA,${index}_INTEGRACAO)`] = result['Integracao'];
          variables[`HASH(DIVIDA,${index}_FILIAL)`] = result['Filial'];
          variables[`HASH(DIVIDA,${index}_DEVEDOR_CODIGO)`] = result['DevedorCodigo'];
          variables[`HASH(DIVIDA,${index}_SALDO)`] = this._arredondarValor(result['SaldoAtualizadoTotal']);
        });

        return this._setVars(context, variables);
      });
  }

  ConsultarParcelasNegociaveis(context, integracao, devedorCodigo, filial, vencimento, contrato) {
    const paramsList = [
      {
        varName: 'HASH(DADOS_CLIENTE,INTEGRACAO)',
        paramName: 'integracao',
        paramValue: integracao,
      },
      {
        varName: 'HASH(DADOS_CLIENTE,CODIGO)',
        paramName: 'devedorCodigo',
        paramValue: devedorCodigo,
      },
      {
        varName: 'HASH(DADOS_CLIENTE,FILIAL)',
        paramName: 'filial',
        paramValue: filial,
      },
      {
        // Vencimento para cálculo dos valores da negociação.
        // Atualmente passa-se a data de realização da negociação.
        varName: 'VENCIMENTO',
        paramName: 'vencimento',
        paramValue: vencimento,
      },
      {
        // Pode ser um ou uma lista separada por virgula.
        // Ex.: "13" ou "12,14,16".
        varName: 'HASH(DADOS_CLIENTE,CONTRATO)',
        paramName: 'contrato',
        paramValue: contrato,
      },
    ];

    const ws = this.deps[MULTICOBRA_WS_SERVICE];

    return this._getParamsOrVars(context, paramsList)
      .then(params => ws.consultarParcelasNegociaveis(context, params))
      .then(results => {
        if(results.length === 1 && results[0]['Mensagem']) {
          throw new Error(results[0]['Mensagem']);
        }

        const variables = {
          'HASH(PARCELA,COUNT)': results.length,
        };

        results.forEach((result, index) => {
          variables[`HASH(PARCELA,${index}_CONTRATO_NUMERO)`] = result['ContratoNumero'];
          variables[`HASH(PARCELA,${index}_PARCELA_NUMERO)`] = result['ParcelaNumero'];
          variables[`HASH(PARCELA,${index}_PARCELA_VENCIMENTO)`] = result['ParcelaVencimento'];
          variables[`HASH(PARCELA,${index}_CHAVE_CONTROLE_NEGOCIACAO)`] = result['ChaveControleNegociacao'];
          variables[`HASH(PARCELA,${index}_CONTRATO_CODIGO)`] = result['ContratoCodigo'];
          variables[`HASH(PARCELA,${index}_PARCELA_CODIGO)`] = result['ParcelaCodigo'];
          variables[`HASH(PARCELA,${index}_CONTRATO_PARCELA_CODIGO)`] = `${result['ContratoCodigo']}-${result['ParcelaCodigo']}`;
          variables[`HASH(PARCELA,${index}_PARCELA_SITUACAO)`] = result['ParcelaSituacao'];
          variables[`HASH(PARCELA,${index}_PARCELA_ATRASO_CALCULO)`] = result['ParcelaAtrasoCalculo'];
          variables[`HASH(PARCELA,${index}_PARCELA_VALOR)`] = this._arredondarValor(result['ParcelaValor']);
          variables[`HASH(PARCELA,${index}_PARCELA_VALOR_DESCONTO)`] = this._arredondarValor(result['ParcelaValorDesconto']);
          variables[`HASH(PARCELA,${index}_PARCELA_SALDO_ATUALIZADO_ORIGINAL)`] = this._arredondarValor(result['ParcelaSaldoAtualizadoOriginal']);
          variables[`HASH(PARCELA,${index}_PARCELA_SALDO_ATUALIZADO)`] = this._arredondarValor(result['ParcelaSaldoAtualizado']);
        });

        return this._setVars(context, variables);
      });
  }

  ListarParcelamentos(context, chaveControleNegociacao, vencimento, entrada, parcelado, contratoParcela) {
    const paramsList = [
      {
        varName: 'HASH(DADOS_CLIENTE,CHAVE_CONTROLE_NEGOCIACAO)',
        paramName: 'chaveControleNegociacao',
        paramValue: chaveControleNegociacao,
      },
      {
        // Data de vencimento para primeira parcela,
        // ou parcela única do parcelamento.
        // AAAA-MM-DD
        varName: 'VENCIMENTO',
        paramName: 'vencimento',
        paramValue: vencimento,
      },
      {
        varName: 'ENTRADA',
        paramName: 'entrada',
        paramValue: entrada,
      },
      {
        // Flag que indica se houve opção por parcelamento (0 ou 1)
        varName: 'PARCELADO',
        paramName: 'parcelado',
        paramValue: parcelado,
      },
      {
        // Pode ser um ou uma lista separada por virgula.
        // Ex.: "135761498-1" ou "135761498-1,135761498-2".
        varName: 'HASH(DADOS_CLIENTE,CONTRATO_PARCELA)',
        paramName: 'contratoParcela',
        paramValue: contratoParcela,
      },
    ];

    const ws = this.deps[MULTICOBRA_WS_SERVICE];

    return this._getParamsOrVars(context, paramsList)
      .then(params => ws.listarParcelamentos(context, params))
      .then(results => {
        if(results.length === 1 && results[0]['Mensagem']) {
          throw new Error(results[0]['Mensagem']);
        }

        const variables = {
          'HASH(NEGOCIACAO,COUNT)': results.length,
        };

        results.forEach((result, index) => {
          variables[`HASH(NEGOCIACAO,${index}_CHAVE_CONTROLE_NEGOCIACAO)`] = result['ChaveControleNegociacao'];
          variables[`HASH(NEGOCIACAO,${index}_CODIGO_PARCELAMENTO)`] = result['Codigo'];
          variables[`HASH(NEGOCIACAO,${index}_DATA_BASE)`] = this._formatarData(result['DataBase']);
          variables[`HASH(NEGOCIACAO,${index}_DATA_VENCIMENTO)`] = this._formatarData(result['DataVencimento']);
          variables[`HASH(NEGOCIACAO,${index}_PLANO)`] = result['Plano'];
          variables[`HASH(NEGOCIACAO,${index}_SALDO)`] = this._arredondarValor(result['Saldo']);
          variables[`HASH(NEGOCIACAO,${index}_SALDO_ORIGINAL)`] = this._arredondarValor(result['SaldoOriginal']);
          variables[`HASH(NEGOCIACAO,${index}_VALOR)`] = this._arredondarValor(result['Valor']);
          variables[`HASH(NEGOCIACAO,${index}_VALOR_BASE)`] = this._arredondarValor(result['ValorBase']);
          variables[`HASH(NEGOCIACAO,${index}_VALOR_DESCONTO)`] = this._arredondarValor(result['ValorDesconto']);
          variables[`HASH(NEGOCIACAO,${index}_VALOR_ENTRADA)`] = this._arredondarValor(result['ValorEntrada']);
          variables[`HASH(NEGOCIACAO,${index}_VALOR_ENTRADA_MINIMO)`] = this._arredondarValor(result['ValorEntradaMinimo']);
          variables[`HASH(NEGOCIACAO,${index}_VALOR_ORIGINAL)`] = this._arredondarValor(result['ValorOriginal']);
          variables[`HASH(NEGOCIACAO,${index}_VALOR_PARCELA)`] = this._arredondarValor(result['ValorParcela']);
        });

        return this._setVars(context, variables);
      });
  }

  SalvarNegociacao(context, chaveControleNegociacao, codigoParcelamento, email, ddd, telefone) {
    const paramsList = [
      {
        varName: 'HASH(DADOS_CLIENTE,CHAVE_CONTROLE_NEGOCIACAO)',
        paramName: 'chaveControleNegociacao',
        paramValue: chaveControleNegociacao,
      },
      {
        varName: 'HASH(DADOS_CLIENTE,CODIGO_PARCELAMENTO)',
        paramName: 'codigoParcelamento',
        paramValue: codigoParcelamento,
      },
      {
        varName: 'HASH(DADOS_CLIENTE,EMAIL)',
        paramName: 'email',
        paramValue: email,
      },
      {
        varName: 'HASH(DADOS_CLIENTE,DDD)',
        paramName: 'ddd',
        paramValue: ddd,
      },
      {
        varName: 'HASH(DADOS_CLIENTE,TELEFONE)',
        paramName: 'telefone',
        paramValue: telefone,
      },
    ];

    const ws = this.deps[MULTICOBRA_WS_SERVICE];

    return this._getParamsOrVars(context, paramsList)
      .then(params => ws.salvarNegociacao(context, params))
      .then(result => {
        if(result['Erro']) {
          throw new Error(result['Mensagem']);
        }

        const variables = {
            'HASH(DADOS_CLIENTE,BOLETO)': result['LinhaDigitavel'],
            'HASH(DADOS_CLIENTE,CODIGO_BOLETO)': result['CodigoBoleto'],
        };

        return this._setVars(context, variables);
      });
  }


  Teste(context) {
    console.log(require('util').inspect(context, { depth: null }));
    return Promise.resolve(context.setVariable("TEST_OK", 1));
  }

  _arredondarValor(valor) {
    return parseFloat(valor || 0).toFixed(2);
  }

  _formatarData(data) {
    const yyyymmdd = /^\d{4}-\d{2}-\d{2}/;

    if(yyyymmdd.test(data)) {
      const parts = data.substr(0,10).split('-');

      return `${parts[2]}/${parts[1]}/${parts[0]}`;
    }

    return data;
  }

  _setVars(context, variables) {
    const variablesArr = Object.keys(variables).map(key => {
      return {
        name: key,
        value: variables[key],
      };
    });

    return this.service.constructor.setVariableMulti(context, variablesArr);
  }

  _getParamsOrVars(context, params) {
    const getVariableMulti = this.service.constructor.getVariableMulti;

    const emptyParams = params.filter(param => !param.paramValue);
    const emptyParamsVars = emptyParams.map(param => param.varName);

    const mapVariables = variables => {
      return variables.reduce((result, variable) => {
        result[variable.name] = variable.value;

        return result;
      }, {});
    };

    const populateParams = variables => {
      return params.reduce((result, param) => {
        const value = param.paramValue ? param.paramValue : variables[param.varName];
        result[param.paramName] = value;

        return result;
      }, {});
    };

    return getVariableMulti(context, emptyParamsVars)
      .then(mapVariables)
      .then(populateParams);
  }

};
