const SYSTEMINTERACT_WS_SERVICE = 0;

module.exports = class {
  constructor() {
    this.deps = ['systeminteract-ws'];
  }

  init(service, config) {
    this.service = service;
    this.config = config;
  }

  SetMailingRetorno(context) {
    const paramsList = [
      { paramName: 'IDMailing', varName: '0' },
      { paramName: 'CodigoCampanha', varName: 'CAMPANHA' },
      { paramName: 'CustomerId', varName: 'HASH(DADOS_CLIENTE,CODIGO_CLIENTE)' },
      { paramName: 'IDTelefoneCliente', varName: '0' },
      { paramName: 'DDD', varName: 'DDD' },
      { paramName: 'Telefone', varName: 'TELEFONE' },
      { paramName: 'CallDate', varName: 'CDR(DataHoraInicio)' },
      { paramName: 'DataHoraAtendeu', varName: 'CDR(DataHoraAtendeu)' },
      { paramName: 'DataHoraFim', varName: 'CDR(DataHoraFim)' },
      { paramName: 'Duracao', varName: 'CDR(duration)' },
      { paramName: 'StatusLigacao', varName: 'CDR(StatusLigacao)' },
      { paramName: 'SubEstrutura', varName: 'CDR(SubStatusURA)' },
      { paramName: 'FaseURA', varName: 'QUADRO' },
      { paramName: 'URLGravacao', varName: 'CDR(PathGravacao)' },
      { paramName: 'BillSec', varName: 'CDR(billsec)' },
    ];

    const ws = this.deps[SYSTEMINTERACT_WS_SERVICE];

    return this._getParamsOrVars(context, paramsList)
      .then(params => ws.SetMailingRetorno(context, {
        IDMailing: 0,
        CodigoCampanha: parseInt(params.CodigoCampanha),
        CustomerId: parseInt(params.CustomerId),
        TelefonesCliente: [
          {
            IDTelefoneCliente: 0,
            Telefone: params.DDD + params.Telefone,
            CallDate: params.CallDate,
            DataHoraAtendeu: params.DataHoraAtendeu,
            DataHoraFim: params.DataHoraFim,
            Duracao: parseInt(params.Duracao),
            StatusLigacao: params.StatusLigacao,
            SubEstrutura: params.SubEstrutura,
            FaseURA: params.FaseURA,
            URLGravacao: params.URLGravacao,
            BillSec: params.BillSec,
          }
        ],
      }))
      .then(result => {
        const outVariables = {
          'SUCESSO': 1,
        };

        return this._setVars(context, outVariables);
      });
  }

  _setVars(context, variables) {
    const variablesArr = Object.keys(variables).map(key => {
      return {
        name: key,
        value: variables[key],
      };
    });

    return this.service.constructor.setVariableMulti(context, variablesArr);
  }

  _getParamsOrVars(context, params) {
    const getVariableMulti = this.service.constructor.getVariableMulti;

    const emptyParams = params.filter(param => !param.paramValue);
    const emptyParamsVars = emptyParams.map(param => param.varName);

    const mapVariables = variables => {
      return variables.reduce((result, variable) => {
        result[variable.name] = variable.value;

        return result;
      }, {});
    };

    const populateParams = variables => {
      return params.reduce((result, param) => {
        const value = param.paramValue ? param.paramValue : variables[param.varName];
        result[param.paramName] = value;

        return result;
      }, {});
    };

    return getVariableMulti(context, emptyParamsVars)
      .then(mapVariables)
      .then(populateParams);
  }

};
