
const INTERVALOR_WS_SERVICE = 0;

const sprintf = require("sprintf-js").sprintf;

const _ = require("lodash");

const sms = require("./sms");

function dateStrToEpoch(dateStr) {
    let dateParts = dateStr.split("-");
    if (!Array.isArray(dateParts) || dateParts.length != 3)
        return null
    return parseInt((new Date(dateParts[2], dateParts[1] - 1, dateParts[0])).getTime() / 1000);
}

function digitosValor(valor) {
    if (typeof valor == "number")
        return sprintf("%0.2f", valor);
    return valor;
}


    module.exports = class extends sms {
        constructor() {
    	       super();
               this.deps = ["intervalor-ws"];
        }

        init(service, config) {
            this.service = service;
            this.config = config;
        }

    PesquisaCliente(context) {
        return this.service.constructor.getVariable(context, "HASH(DADOS_CLIENTE,CPF)").then(cpf => {
            return this.deps[INTERVALOR_WS_SERVICE].pesquisaCliente(context, {
                dicParamPesqCliente: {
                    sCnpjGrupoEconomico: cpf
                }
            });
        }).then(res => {
            let varvalues = new Array();
            varvalues.push({name: "HASH(WS_DADOS_CLIENTE,eStatusGeral)", value: res.response.eStatusGeral});
            if (res.response.eStatusGeral == "ENCONTRADO") {
                if (!Array.isArray(res.response.arClientes) || !res.response.arClientes.length)
                    throw new Error("Array de clientes inválido");
                if (res.response.arClientes[0].sNome)
                    varvalues.push({name: "HASH(WS_DADOS_CLIENTE,sNome)", value: res.response.arClientes[0].sNome});
                if (res.response.arClientes[0].dDataNascimento)
                    varvalues.push({name: "HASH(WS_DADOS_CLIENTE,dDataNascimento)", value: res.response.arClientes[0].dDataNascimento});
                if (Array.isArray(res.response.arClientes[0].arCarteiras) && res.response.arClientes[0].arCarteiras.length) {
                    varvalues.push({name: "HASH(CARTEIRAS,arCarteiras)", values: res.response.arClientes[0].arCarteiras.map(carteira => carteira.nId).join("|")});
                    res.response.arClientes[0].arCarteiras.forEach(carteira => {
                        if (isNaN(carteira.nId))
                            throw new Error("ID Carteira inválido");
                        if (carteira.sNome)
                            varvalues.push({name: sprintf("HASH(CARTEIRAS,arCarteiras_%d_sNome)", carteira.nId), value: carteira.sNome});
                        if (carteira.sCnpj)
                            varvalues.push({name: sprintf("HASH(CARTEIRAS,arCarteiras_%d_sCnpj)", carteira.nId), value: carteira.sCnpj});
                    });
                }
                if (Array.isArray(res.response.arClientes[0].arTelefone)) {
                    varvalues.push({name: "HASH(WS_DADOS_CLIENTE,arTelefone)", value: res.response.arClientes[0].arTelefone.length});
                    res.response.arClientes[0].arTelefone.forEach((telefone, index) => {
                        varvalues.push({name: sprintf("HASH(WS_DADOS_CLIENTE,arTelefone_%d_AceitaReceberSms)", index), value: telefone.AceitaReceberSms ? 1 : 0});
                        varvalues.push({name: sprintf("HASH(WS_DADOS_CLIENTE,arTelefone_%d_Ativo)", index), value: telefone.Ativo ? 1 : 0});
                        varvalues.push({name: sprintf("HASH(WS_DADOS_CLIENTE,arTelefone_%d_Ddd)", index), value: telefone.Ddd});
                        varvalues.push({name: sprintf("HASH(WS_DADOS_CLIENTE,arTelefone_%d_Telefone)", index), value: telefone.Telefone});
                        if (telefone.Ramal)
                            varvalues.push({name: sprintf("HASH(WS_DADOS_CLIENTE,arTelefone_%d_Ramal)", index), value: telefone.Ramal});
                    });
                }
                if (Array.isArray(res.response.arClientes[0].arEmail)) {
                    varvalues.push({name: "HASH(WS_DADOS_CLIENTE,arEmail)", value: res.response.arClientes[0].arEmail.length});
                    res.response.arClientes[0].arEmail.forEach((email, index) => {
                        varvalues.push({name: sprintf("HASH(WS_DADOS_CLIENTE,arEmail_%d_AceitaReceberEmail)", index), value: email.AceitaReceberEmail ? 1 : 0});
                        varvalues.push({name: sprintf("HASH(WS_DADOS_CLIENTE,arEmail_%d_Ativo)", index), value: email.Ativo ? 1 : 0});
                        varvalues.push({name: sprintf("HASH(WS_DADOS_CLIENTE,arEmail_%d_Email)", index), value: email.Email});
                    });
                }
            }
            return this.service.constructor.setVariableMulti(context, varvalues);
        });
    }

    AgrupamentoDivida(context, idCarteira) {
        idCarteira = parseInt(idCarteira);
        if (isNaN(idCarteira))
            return Promise.reject(new Error("Parâmetro idCarteira (ARG1) inválido"));
        return this.service.constructor.getVariable(context, "HASH(DADOS_CLIENTE,CPF)").then(cpf => {
            return this.deps[INTERVALOR_WS_SERVICE].agrupamentoDivida(context, {
                nIdCarteira: idCarteira,
                dicParamPesqCliente: {
                    sCnpjGrupoEconomico: cpf
                }
            });
        }).then(res => {
            let varvalues = new Array();
            varvalues.push({name: "HASH(DIVIDA,eStatusGeral)", value: res.response.eStatusGeral});
            if (res.response.eStatusGeral == "ENCONTRADO") {
                if (!Array.isArray(res.response.arAgrupamentos) || !res.response.arAgrupamentos.length)
                    throw new Error("Array de agrupamento de dividas inválido");
                varvalues.push({name: "HASH(DIVIDA,arAgrupamentos)", value: res.response.arAgrupamentos.map(agrupamento => agrupamento.nIdAgrupamento).join("|")});
                res.response.arAgrupamentos.forEach(agrupamento => {
                    varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_eStatusAgrupamento)", agrupamento.nIdAgrupamento), value: agrupamento.eStatusAgrupamento});
                    varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_nIdAcordo)", agrupamento.nIdAgrupamento), value: agrupamento.nIdAcordo});
                    if (Array.isArray(agrupamento.arDividas)) {
                        varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos)", agrupamento.nIdAgrupamento), value: _.uniq(_.concat.apply(_, agrupamento.arDividas.map(divida => divida.arContratos.map(contrato => contrato.nIdContrato)))).join("|")});
                        agrupamento.arDividas.forEach(divida => {
                            if (Array.isArray(divida.arContratos)) {
                                divida.arContratos.forEach(contrato => {
                                    varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_sContrato)", agrupamento.nIdAgrupamento, contrato.nIdContrato), value: contrato.sContrato});
                                    varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_nValorContrato)", agrupamento.nIdAgrupamento, contrato.nIdContrato), value: digitosValor(contrato.nValorContrato)});
                                    varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_sProduto)", agrupamento.nIdAgrupamento, contrato.nIdContrato), value: contrato.sProduto});
                                    if (Array.isArray(contrato.arParcelas) && contrato.arParcelas.length) {
                                        varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_arParcelas)", agrupamento.nIdAgrupamento, contrato.nIdContrato), value: contrato.arParcelas.map(parcela => parcela.nIdParcela).join("|")});
                                        let ValorTotalParcelas =0;
                                        contrato.arParcelas.forEach(parcela => {
                                            varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_arParcelas_%d_nParcela)", agrupamento.nIdAgrupamento, contrato.nIdContrato, parcela.nIdParcela), value: parcela.nParcela});
                                            varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_arParcelas_%d_dDataVencimento)", agrupamento.nIdAgrupamento, contrato.nIdContrato, parcela.nIdParcela), value: dateStrToEpoch(parcela.dDataVencimento)});
                                            varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_arParcelas_%d_nAtraso)", agrupamento.nIdAgrupamento, contrato.nIdContrato, parcela.nIdParcela), value: parcela.nAtraso});
                                            varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_arParcelas_%d_nValorParcelaOriginal)", agrupamento.nIdAgrupamento, contrato.nIdContrato, parcela.nIdParcela), value: digitosValor(parcela.nValorParcelaOriginal)});
                                            varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_arParcelas_%d_nValorParcelaAtualizado)", agrupamento.nIdAgrupamento, contrato.nIdContrato, parcela.nIdParcela), value: digitosValor(parcela.nValorParcelaAtualizado)});
                                            ValorTotalParcelas = ValorTotalParcelas + parcela.nValorParcelaAtualizado;
                                        });
                                        varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_QtdParcelasTotal)", agrupamento.nIdAgrupamento, contrato.nIdContrato ), value: contrato.arParcelas.length});
                                        varvalues.push({name: sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_ValorParcelasTotal)", agrupamento.nIdAgrupamento, contrato.nIdContrato ), value: digitosValor(ValorTotalParcelas)});
                                    }
                                });
                            }
                        });
                    }
                });
            }
            return this.service.constructor.setVariableMulti(context, varvalues);
        });
    }

    CondicoesNegociacao(context, idCarteira, idAgrupamento, idContrato, pIdParcela) {
        idCarteira = parseInt(idCarteira);
        idAgrupamento = parseInt(idAgrupamento);
        idContrato = parseInt(idContrato);
        if (isNaN(idCarteira))
            return Promise.reject(new Error("Parâmetro idCarteira (ARG1) inválido"));
        if (isNaN(idAgrupamento))
            return Promise.reject(new Error("Parâmetro idAgrupamento (ARG2) inválido"));
        if (isNaN(idContrato))
            return Promise.reject(new Error("Parâmetro idContrato (ARG3) inválido"));
        return this.service.constructor.getVariableMulti(context, ["HASH(DADOS_CLIENTE,CNPJ_CREDOR)", "HASH(DADOS_CLIENTE,CPF)"]).then(varvalues => {
            if (varvalues[0].value == null)
                return this.service.constructor.getVariable(context, sprintf("HASH(CARTEIRAS,%d_sCnpj)", idCarteira)).then(value => {
                    varvalues[0].value = value;
                    return varvalues;
                });
            else
                return varvalues;
        }).then(varvalues => {
            return (pIdParcela ? Promise.resolve(pIdParcela) : this.service.constructor.getVariable(context, sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_arParcelas)", idAgrupamento, idContrato))).then(idParcelaLista => {
                return idParcelaLista.split("|").map(idParcela => parseInt(idParcela));
            }).then(idParcelaLista => {
                idParcelaLista.forEach(idParcela => {
                    if (isNaN(idParcela))
                        throw new Error("idParcela inválido");
                });
                return this.deps[INTERVALOR_WS_SERVICE].condicoesNegociacao(context, {
                    sCnpjCredor: varvalues[0].value,
                    sCpfCliente: varvalues[1].value,
                    nIdAgrupamento: idAgrupamento,
                    arIdParcelas: idParcelaLista
                });
            });
        }).then(res => {
            let varvalues = new Array();
            varvalues.push({name: "HASH(NEGOCIACAO,eStatusGeral)", value: res.response.eStatusGeral});
            if (res.response.eStatusGeral == "POSSUI_CONDICOES") {
                varvalues.push({name: "HASH(NEGOCIACAO,nMaxParcelas)", value: res.response.nMaxParcelas});
                varvalues.push({name: "HASH(NEGOCIACAO,arDatasVencimentos)", value: res.response.arDatasVencimentos.map(dateStrToEpoch).join("|")});
                varvalues.push({name: "HASH(NEGOCIACAO,nValorTotal)", value: digitosValor(res.response.nValorTotal)});
                varvalues.push({name: "HASH(NEGOCIACAO,nValorAVista)", value: digitosValor(res.response.nValorAVista)});
                varvalues.push({name: "HASH(NEGOCIACAO,dDataVenctoAVista)", value: dateStrToEpoch(res.response.dDataVenctoAVista)});
                if (res.response.nValorDesconto)
                    varvalues.push({name: "HASH(NEGOCIACAO,nValorDesconto)", value: digitosValor(res.response.nValorDesconto)});
                if (res.response.nPercDesconto)
                    varvalues.push({name: "HASH(NEGOCIACAO,nPercDesconto)", value: res.response.nPercDesconto});
                varvalues.push({name: "HASH(NEGOCIACAO,nIdProposta)", value: res.response.nIdProposta});
                varvalues.push({name: "HASH(NEGOCIACAO,nValorParcelaOriginal)", value: digitosValor(res.response.nValorParcelaOriginal)});
                varvalues.push({name: "HASH(NEGOCIACAO,nValorJuros)", value: digitosValor(res.response.nValorJuros)});
                varvalues.push({name: "HASH(NEGOCIACAO,nValorMulta)", value: digitosValor(res.response.nValorMulta)});
                varvalues.push({name: "HASH(NEGOCIACAO,nValorPermanencia)", value: digitosValor(res.response.nValorPermanencia)});
                varvalues.push({name: "HASH(NEGOCIACAO,nValorHonorario)", value: digitosValor(res.response.nValorHonorario)});
                // ParametroAtualizacaoDivida
                if (typeof res.response.ParametroAtualizacaoDivida == "object") {
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Juros)", value: res.response.ParametroAtualizacaoDivida.Juros});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_JurosMinimo)", value: res.response.ParametroAtualizacaoDivida.JurosMinimo});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_JurosMaximo)", value: res.response.ParametroAtualizacaoDivida.JurosMaximo});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Honorarios)", value: res.response.ParametroAtualizacaoDivida.Honorarios});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_HonorariosMinimo)", value: res.response.ParametroAtualizacaoDivida.HonorariosMinimo});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_HonorariosMaximo)", value: res.response.ParametroAtualizacaoDivida.HonorariosMaximo});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Desconto)", value: res.response.ParametroAtualizacaoDivida.Desconto});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_DescontoMinimo)", value: res.response.ParametroAtualizacaoDivida.DescontoMinimo});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_DescontoMaximo)", value: res.response.ParametroAtualizacaoDivida.DescontoMaximo});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Multa)", value: res.response.ParametroAtualizacaoDivida.Multa});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_MultaMinima)", value: res.response.ParametroAtualizacaoDivida.MultaMinima});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_MultaMaxima)", value: res.response.ParametroAtualizacaoDivida.MultaMaxima});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Permanencia)", value: res.response.ParametroAtualizacaoDivida.Permanencia});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_PermanenciaMinima)", value: res.response.ParametroAtualizacaoDivida.PermanenciaMinima});
                    varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_PermanenciaMaxima)", value: res.response.ParametroAtualizacaoDivida.PermanenciaMaxima});
                    if (typeof res.response.ParametroAtualizacaoDivida.InfoAdicional == "object") {
                        varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_InfoAdicional_TipoCalculo)", value: res.response.ParametroAtualizacaoDivida.InfoAdicional.TipoCalculo});
                        varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_InfoAdicional_ValorMinimo)", value: res.response.ParametroAtualizacaoDivida.InfoAdicional.ValorMinimo});
                        varvalues.push({name: "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_InfoAdicional_PercentualEntrada)", value: res.response.ParametroAtualizacaoDivida.InfoAdicional.PercentualEntrada});
                    }
                }
            }
            return this.service.constructor.setVariableMulti(context, varvalues);
        });
    }

    CalculoCondicaoNegociacao(context, idCarteira, idAgrupamento, idContrato, qtdParcelas, pIdParcela, dataVencimento) {
        idCarteira = parseInt(idCarteira);
        idAgrupamento = parseInt(idAgrupamento);
        idContrato = parseInt(idContrato);
        qtdParcelas = parseInt(qtdParcelas);
        dataVencimento = parseInt(dataVencimento);
        if (isNaN(idCarteira))
            return Promise.reject(new Error("Parâmetro idCarteira (ARG1) inválido"));
        if (isNaN(idAgrupamento))
            return Promise.reject(new Error("Parâmetro idAgrupamento (ARG2) inválido"));
        if (isNaN(idContrato))
            return Promise.reject(new Error("Parâmetro idContrato (ARG3) inválido"));
        if (isNaN(qtdParcelas))
            return Promise.reject(new Error("Parâmetro qtdParcelas (ARG4) inválido"));
        if (isNaN(dataVencimento))
            return Promise.reject(new Error("Parâmetro dataVencimento (ARG6) inválido"));
        dataVencimento = new Date(dataVencimento * 1000);
        dataVencimento = dataVencimento.toISOString().substr(0, 10).split('-').reverse().join('-');
        return this.service.constructor.getVariableMulti(context, ["HASH(DADOS_CLIENTE,CNPJ_CREDOR)", "HASH(DADOS_CLIENTE,CPF)"]).then(varvalues => {
            if (varvalues[0].value == null)
                return this.service.constructor.getVariable(context, sprintf("HASH(CARTEIRAS,%d_sCnpj)", idCarteira)).then(value => {
                    varvalues[0].value = value;
                    return varvalues;
                });
            else
                return varvalues;
        }).then(varvalues => {
            return (pIdParcela ? Promise.resolve(pIdParcela) : this.service.constructor.getVariable(context, sprintf("HASH(DIVIDA,arAgrupamentos_%d_arContratos_%d_arParcelas)", idAgrupamento, idContrato))).then(idParcelaLista => {
                return idParcelaLista.split("|").map(idParcela => parseInt(idParcela));
            }).then(idParcelaLista => {
                idParcelaLista.forEach(idParcela => {
                    if (isNaN(idParcela))
                        throw new Error("idParcela inválido");
                });
                return this.service.constructor.getVariableMulti(context, [
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Juros)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_JurosMinimo)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_JurosMaximo)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Honorarios)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_HonorariosMinimo)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_HonorariosMaximo)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Desconto)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_DescontoMinimo)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_DescontoMaximo)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Multa)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_MultaMinima)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_MultaMaxima)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_Permanencia)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_PermanenciaMinima)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_PermanenciaMaxima)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_InfoAdicional_TipoCalculo)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_InfoAdicional_ValorMinimo)",
                    "HASH(NEGOCIACAO,ParametroAtualizacaoDivida_InfoAdicional_PercentualEntrada)"
                ]).then(parametroAtualizacaoDividaLista => {
                    let parametroAtualizacaoDividaObj = {};

                    parametroAtualizacaoDividaLista.forEach(parametroAtualizacaoDivida => {
                        let m = parametroAtualizacaoDivida.name.match(/^HASH\(NEGOCIACAO,ParametroAtualizacaoDivida_(.+)\)$/);

                        if (m) {
                            if (m[1].substring(0, 14) == "InfoAdicional_") {
                                if (typeof parametroAtualizacaoDividaObj.InfoAdicional != "object")
                                    parametroAtualizacaoDividaObj.InfoAdicional = {};
                                parametroAtualizacaoDividaObj.InfoAdicional[m[1].substring(14)] = parametroAtualizacaoDivida.value;
                            } else {
                                parametroAtualizacaoDividaObj[m[1]] = parametroAtualizacaoDivida.value;
                            }
                        }
                    });
                    return this.deps[INTERVALOR_WS_SERVICE].calculoCondicaoNegociacao(context, {
                        sCnpjCredor: varvalues[0].value,
                        sCpfCliente: varvalues[1].value,
                        nIdAgrupamento: idAgrupamento,
                        nQtdParcelas: qtdParcelas,
                        dDataVencimento: dataVencimento,
                        arIdParcelas: idParcelaLista,
                        dicParametrosAtualizacaoDivida: parametroAtualizacaoDividaObj
                    });
                });
            });
        }).then(res => {
            let varvalues = new Array();
            varvalues.push({name: "HASH(CALCULO_NEGOCIACAO,eStatusGeral)", value: res.response.eStatusGeral});
            if (res.response.eStatusGeral == "PROPOSTA_GERADA") {
                varvalues.push({name: "HASH(CALCULO_NEGOCIACAO,nValorParcela)", value: digitosValor(res.response.nValorParcela)});
                varvalues.push({name: "HASH(CALCULO_NEGOCIACAO,nValorTotal)", value: digitosValor(res.response.nValorTotal)});
                varvalues.push({name: "HASH(CALCULO_NEGOCIACAO,nValorTotalComDesconto)", value: digitosValor(res.response.nValorTotalComDesconto)});
                varvalues.push({name: "HASH(CALCULO_NEGOCIACAO,nValorTotalDesconto)", value: digitosValor(res.response.nValorTotalDesconto)});
                varvalues.push({name: "HASH(CALCULO_NEGOCIACAO,nPercTotalDesconto)", value: res.response.nPercTotalDesconto});
                varvalues.push({name: "HASH(CALCULO_NEGOCIACAO,nIdProposta)", value: res.response.nIdProposta});
            }
            return this.service.constructor.setVariableMulti(context, varvalues);
        });
    }

    EmissaoAcordo(context, idCarteira, idProposta) {
        idCarteira = parseInt(idCarteira);
        idProposta = parseInt(idProposta);
        if (isNaN(idCarteira))
            return Promise.reject(new Error("Parâmetro idCarteira (ARG1) inválido"));
        if (isNaN(idProposta))
            return Promise.reject(new Error("Parâmetro idProposta (ARG2) inválido"));
        return this.service.constructor.getVariableMulti(context, ["HASH(DADOS_CLIENTE,CNPJ_CREDOR)", "HASH(DADOS_CLIENTE,CPF)"]).then(varvalues => {
            if (varvalues[0].value == null)
                return this.service.constructor.getVariable(context, sprintf("HASH(CARTEIRAS,%d_sCnpj)", idCarteira)).then(value => {
                    varvalues[0].value = value;
                    return varvalues;
                });
            else
                return varvalues;
        }).then(varvalues => {
            return this.deps[INTERVALOR_WS_SERVICE].emissaoAcordo(context, {
                sCnpjCredor: varvalues[0].value,
                sCpfCliente: varvalues[1].value,
                nIdProposta: idProposta
            });
        }).then(res => {
            let varvalues = new Array();
            varvalues.push({name: "HASH(ACORDO,eStatusGeral)", value: res.response.eStatusGeral});
            if (res.response.eStatusGeral == "ACORDO_GERADO") {
                varvalues.push({name: "HASH(ACORDO,nIdAcordo)", value: res.response.nIdAcordo});
                if (res.response.sBoletoLinhaDigitavel)
                    varvalues.push({name: "HASH(ACORDO,sBoletoLinhaDigitavel)", value: res.response.sBoletoLinhaDigitavel});
                if (res.response.dBoletoDataVencimento)
                    varvalues.push({name: "HASH(ACORDO,dBoletoDataVencimento)", value: dateStrToEpoch(res.response.dBoletoDataVencimento)});
                if (res.response.nBoletoValor)
                    varvalues.push({name: "HASH(ACORDO,nBoletoValor)", value: res.response.nBoletoValor});
            } else if (res.response.eStatusGeral == "ACORDO_EM_PROCESSAMENTO") {
                varvalues.push({name: "HASH(ACORDO,nIdAcordo)", value: res.response.nIdAcordo});
            }
            return this.service.constructor.setVariableMulti(context, varvalues);
        });
    }

    SegundaViaBoleto(context, idCarteira, idAcordo) {
        idCarteira = parseInt(idCarteira);
        idAcordo = parseInt(idAcordo);
        if (isNaN(idCarteira))
            return Promise.reject(new Error("Parâmetro idCarteira (ARG1) inválido"));
        if (isNaN(idAcordo))
            return Promise.reject(new Error("Parâmetro idAcordo (ARG2) inválido"));
        return this.service.constructor.getVariableMulti(context, ["HASH(DADOS_CLIENTE,CNPJ_CREDOR)", "HASH(DADOS_CLIENTE,CPF)"]).then(varvalues => {
            if (varvalues[0].value == null)
                return this.service.constructor.getVariable(context, sprintf("HASH(CARTEIRAS,%d_sCnpj)", idCarteira)).then(value => {
                    varvalues[0].value = value;
                    return varvalues;
                });
            else
                return varvalues;
        }).then(varvalues => {
            return this.deps[INTERVALOR_WS_SERVICE].segundaViaBoleto(context, {
                sCnpjCredor: varvalues[0].value,
                sCpfCliente: varvalues[1].value,
                nIdAcordo: idAcordo
            });
        }).then(res => {
            let varvalues = new Array();
            varvalues.push({name: "HASH(ACORDO,eStatusGeral)", value: res.response.eStatusGeral});
            if (res.response.eStatusGeral == "LOCALIZADO") {
                varvalues.push({name: "HASH(ACORDO,nIdAcordo)", value: res.response.nIdAcordo});
                varvalues.push({name: "HASH(ACORDO,sBoletoLinhaDigitavel)", value: res.response.sBoletoLinhaDigitavel});
                varvalues.push({name: "HASH(ACORDO,dBoletoDataVencimento)", value: dateStrToEpoch(res.response.dBoletoDataVencimento)});
                varvalues.push({name: "HASH(ACORDO,nBoletoValor)", value: res.response.nBoletoValor});
            }
            return this.service.constructor.setVariableMulti(context, varvalues);
        });
    }

    sms(context, telefone, mensagem) {
        return this.service.constructor.getVariable(context, "SERVICE_SMS").then(service => {
            if (service == null) {
                return this.service.constructor.getVariableMulti(context, ["HASH(DADOS_CLIENTE,CPF)", "HASH(DADOS_CLIENTE,NOME_VOCALIZAR)"]).then(varvalues => {
                    return {
                        cpf: varvalues[0].value,
                        nome: varvalues[1].value
                    };
                }).then(sms => {
                    if (telefone) {
                        sms.telefone = telefone;
                        return sms;
                    }
                    return this.service.constructor.getVariable(context, "NUMERO_SMS").then(value => {
                        if (value == null)
                            throw new Error("Variavel NUMERO_SMS nao setada");
                        sms.telefone = value;
                        return sms;
                    });
                }).then(sms => {
                    if (mensagem) {
                        sms.mensagem = mensagem;
                        return sms;
                    }
                    return this.service.constructor.getVariable(context, "MENSAGEM_SMS").then(value => {
                        if (value == null)
                            throw new Error("Variavel MENSAGEM_SMS nao setada");
                        sms.mensagem = value;
                        return sms;
                    });
                }).then(sms => {
                    sms.ddd = sms.telefone.substring(0, 2);
                    sms.telefone = sms.telefone.substring(2);
                    return this.deps[INTERVALOR_WS_SERVICE].nocaiEnviaSms(context, sms);
                }).then(res => {
                    let varvalues = new Array();

                    if (Array.isArray(res.response.Activities) && res.response.Activities.length) {
                        varvalues.push({name: "STATUS_SMS", value: res.response.Activities[0].Status});
                        varvalues.push({name: "ID_SMS", value: res.response.Activities[0].Id});
                    } else {
                        varvalues.push({name: "STATUS_SMS", value: 0});
                    }
                    return this.service.constructor.setVariableMulti(context, varvalues);
                });
            }
//            return super(context, telefone, mensagem, service);
            //return this.service.constructor.setVariableMulti(context, varvalues);
        });
    }

    SendActionConsulta (context, idInstitution, idActionType, idChannel) {
        // Variaveis do asterisk X parametros da chamada
        const requiredVariables = [
            'HASH(DADOS_CLIENTE,CPF)', // Document
            'HASH(PARAMETERS,DATA_VENCIMENTO)', // Parameters.data_vencimento
            'HASH(PARAMETERS,DESCONTO)', // Parameters.desconto
            'HASH(PARAMETERS,PRAZO)', // Parameters.prazo
            'NUMERO_SMS', // Celular com ddd (ex.: 11999024200)
        ];

        return this.service.constructor.getVariableMulti(context, requiredVariables).then(inVariables => {
                const telefone = inVariables[4].value;
                const telefoneDDD = telefone.substring(0,2);
                const telefoneNumero = telefone.substring(2);

                // parametros da chamada
                const params = {
                    Document: inVariables[0].value,
                    IdInstitution: idInstitution,
                    IdActionType: idActionType,
                    Parameters: {
                        data_vencimento: inVariables[1].value,
                        desconto: inVariables[2].value,
                        prazo: inVariables[3].value,
                    },
                    IsAsync: false,
                    IdChannel: idChannel,
                    Attendant: 'nocai',
                    DataAccess1: 55,
                    DataAccess2: telefoneDDD,
                    DataAccess3: telefoneNumero,
                };

                return this.deps[INTERVALOR_WS_SERVICE].nocaiSendAction(context, params);
            })
            .then(res => {
                // retorno de variaveis para o asterisk
                const outVariables = [
                    { name: 'HASH(PROPOSTA,VALOR_TOTAL)', value: res.response['valor_total'] },
                    { name: 'HASH(PROPOSTA,QTD_PARCELA_MIN)', value: res.response['qtd_parcela_min'] },
                    { name: 'HASH(PROPOSTA,QTD_PARCELA_MAX)', value: res.response['qtd_parcela_max'] },
                    { name: 'HASH(PROPOSTA,VALOR_POR_PARCELA)', value: res.response['valor_por_parcela'] },
                    { name: 'HASH(PROPOSTA,ID_ACORDO_PREVIO)', value: res.response['id_acordo_previo'] },
                ];
                //return super(context, outVariables);
                return this.service.constructor.setVariableMulti(context, outVariables);
            });
    }
  
    SendActionEmissao (context, idInstitution, idActionType, idChannel) {
    // Variaveis do asterisk X parametros da chamada
    const requiredVariables = [
      'HASH(DADOS_CLIENTE,CPF)', // Document
      'HASH(PARAMETERS,DATA_VENCIMENTO)', // Parameters.data_vencimento
      'HASH(PARAMETERS,DESCONTO)', // Parameters.desconto
      'HASH(PARAMETERS,PRAZO)', // Parameters.prazo
      'NUMERO_SMS', // Celular com ddd (ex.: 11999024200)
    ];
    
    return this.service.constructor.getVariableMulti(context, requiredVariables).then(inVariables => {
      const telefone = inVariables[4].value;
      const telefoneDDD = telefone.substring(0,2);
      const telefoneNumero = telefone.substring(2);
      
      // parametros da chamada
      const params = {
        Document: inVariables[0].value,
        IdInstitution: idInstitution,
        IdActionType: idActionType,
        Parameters: {
          data_proposta: inVariables[1].value,
          desconto: inVariables[2].value,
          prazo: inVariables[3].value,
        },
        IsAsync: false,
        IdChannel: idChannel,
        Attendant: 'nocai',
        DataAccess1: 55,
        DataAccess2: telefoneDDD,
        DataAccess3: telefoneNumero,
      };
      
      return this.deps[INTERVALOR_WS_SERVICE].nocaiSendAction(context, params);
    })
    .then(res => {
      // retorno de variaveis para o asterisk
      const outVariables = [
        { name: 'HASH(ACORDO,MENSAGEM_SMS)', value: res.response['GeneratedMessage'] },
        { name: 'HASH(ACORDO,STATUS)', value: res.response['status'] },
      ];
      //return super(context, outVariables);
      return this.service.constructor.setVariableMulti(context, outVariables);
    });
  }

}
