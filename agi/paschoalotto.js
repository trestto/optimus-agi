
const PASCHOALOTTO_WS_SERVICE = 0;
// const AGI_SOUND_SERVICE = 1;

const sprintf = require("sprintf-js").sprintf;

const sms = require("./sms");

function funcGetVar(response) {
    if (response.code == 200) {
        let m = /^([01]) \((.*)\)/.exec(response.result);
        if (m && parseInt(m[1]))
            return m[2];
    }
    return null;
}

function dadosNegociacaoOpcao(context) {
    let dadosReq = {};
    return this.service.constructor.getVariableMulti(
        context,
        ["HASH(DADOS_CLIENTE,COD_CARTEIRA)", "HASH(DADOS_CLIENTE,CPF)", "HASH(DADOS_CLIENTE,CONTRATO)",
        "HASH(DADOS_CLIENTE,GRUPO_NEGOCIACAO)", "HASH(DADOS_CLIENTE,TIPO_PESSOA)", "HASH(DADOS_CLIENTE,PLANO)"]
    ).then((varvalues) => {
        varvalues.forEach((variable) => {
            switch (variable.name) {
            case "HASH(DADOS_CLIENTE,COD_CARTEIRA)":        dadosReq.codCarteira = variable.value;      break;
            case "HASH(DADOS_CLIENTE,CPF)":                 dadosReq.numeroDocumento = variable.value;  break;
            case "HASH(DADOS_CLIENTE,CONTRATO)":            dadosReq.NegociacaoOpcaoContratos = [{CodContrato: variable.value}];    break;
            case "HASH(DADOS_CLIENTE,GRUPO_NEGOCIACAO)":    dadosReq.codGrupoNeg = variable.value;      break;
            case "HASH(DADOS_CLIENTE,TIPO_PESSOA)":         dadosReq.tipoPessoa = variable.value;       break;
            case "HASH(DADOS_CLIENTE,PLANO)":               dadosReq.Plano = variable.value;            break;
            }
        });
        return dadosReq;
    });
}

module.exports = class extends sms {
    constructor() {
	super();
        this.deps = ["paschoalotto-ws"];
    }

    init(service, config) {
        this.service = service;
        this.config = config;
    }

    NegociacaoOpcao(context, dataHoraNegociacao) {
        return dadosNegociacaoOpcao.call(this, context).then(dadosReq => {
            let m = /^(\d{2})\/(\d{2})\/(\d{4})$/.exec(dataHoraNegociacao);
            if (m) {
                m[1] = parseInt(m[1]);
                m[2] = parseInt(m[2]) - 1;
                m[3] = parseInt(m[3]);
            }
            if (isNaN(m[1]) || isNaN(m[2]) || isNaN(m[3]))
                m = null;
            let dt = m ? new Date(m[3], m[2], m[1]) : new Date();
            dadosReq.DiaVenc = dt.getDate();
            return dadosReq;
        }).then(dadosReq => this.deps[PASCHOALOTTO_WS_SERVICE].negociacaoOpcao(context, dadosReq)).then((res) => {
            let m, reDate = /Date\((\d+)(?:-(\d+))?\)/;
            let varvalues = new Array();
            if (res.response.NegociacaoOpcoes instanceof Array) {
                let i;
                for (i = 0; i < res.response.NegociacaoOpcoes.length; i++) {
                    varvalues.push({name: sprintf("HASH(NEGOCIACAO,%d_COD_OPCAO)", i), value: res.response.NegociacaoOpcoes[i].CodOpcao});
                    varvalues.push({name: sprintf("HASH(NEGOCIACAO,%d_VALOR_ENTRADA)", i), value: res.response.NegociacaoOpcoes[i].VlEntrada});
                    m = reDate.exec(res.response.NegociacaoOpcoes[i].DtEntrada);
                    if (m) varvalues.push({name: sprintf("HASH(NEGOCIACAO,%d_DATA_ENTRADA)", i), value: parseInt(m[1] / 1000)});
                    varvalues.push({name: sprintf("HASH(NEGOCIACAO,%d_QTD_PARCELAS)", i), value: res.response.NegociacaoOpcoes[i].QtdParcela});
                    varvalues.push({name: sprintf("HASH(NEGOCIACAO,%d_VALOR_PARCELA)", i), value: res.response.NegociacaoOpcoes[i].VlParcela});
                    m = reDate.exec(res.response.NegociacaoOpcoes[i].DtParcela);
                    if (m) varvalues.push({name: sprintf("HASH(NEGOCIACAO,%d_DATA_PARCELA)", i), value: parseInt(m[1] / 1000)});
                    varvalues.push({name: sprintf("HASH(NEGOCIACAO,%d_VENCIMENTO)", i), value: res.response.NegociacaoOpcoes[i].DiaVenc});

                }
                if (i) {
                    varvalues.push({name: "HASH(NEGOCIACAO,COUNT)", value: i});
                    return this.service.constructor.setVariableMulti(context, varvalues);
                }
                throw new Error("No avaliable option found from API Response");
            } else if (typeof res.response.ResponseStatus == "object" && res.response.ResponseStatus.ErrorCode && res.response.ResponseStatus.Message) {
                throw new Error(res.response.ResponseStatus.Message);
            }
            throw new Error("Unhandler API Response");
        });
    }

    CriarAcordo(context, codOpcao) {
        return context.getVariable("HASH(DADOS_CLIENTE,COD_CARTEIRA)").then((response) => {
            let dadosReq = {codOpcao: codOpcao};
            if (response.code == 200) {
                let m = /^([01]) \((.*)\)/.exec(response.result);
                if (m && parseInt(m[1]))
                    dadosReq.codCarteira = m[2];
            }
            return this.deps[PASCHOALOTTO_WS_SERVICE].criarAcordo(context, dadosReq);
        }).then((res) => {
            let acordo = res.response;
            if (Array.isArray(acordo))
                acordo = acordo.find(item => "CodAcordo" in item);
            if (typeof acordo != "object" || !acordo.CodAcordo)
                throw new Error("ERROR: Codigo do Acordo não retornado");
            return context.setVariable("HASH(DADOS_CLIENTE,COD_ACORDO)", acordo.CodAcordo);
        });
    }

    CriarBoleto(context, codAcordo, codCarteira) {
        let dadosReq = {
            codAcordo: codAcordo,
            codCarteira: codCarteira
        };
        let varnames = new Array();

        if (!dadosReq.codAcordo)
            varnames.push("HASH(DADOS_CLIENTE,COD_ACORDO)");
        if (!dadosReq.codCarteira)
            varnames.push("HASH(DADOS_CLIENTE,COD_CARTEIRA)");

        return this.service.constructor.getVariableMulti(context, varnames).then((varvalues) => {
            varvalues.forEach((variable) => {
                switch (variable.name) {
                case "HASH(DADOS_CLIENTE,COD_ACORDO)":      dadosReq.codAcordo = variable.value;    break;
                case "HASH(DADOS_CLIENTE,COD_CARTEIRA)":    dadosReq.codCarteira = variable.value;  break;
                }
            });
            return this.deps[PASCHOALOTTO_WS_SERVICE].criarBoleto(context, dadosReq);
        }).then((res) => {
            if (typeof res.response != "object" || !res.response.LinhaDigit)
                throw new Error("ERROR: Linha Digitável não retornado");
            return context.setVariable("HASH(DADOS_CLIENTE,BOLETO)", res.response.LinhaDigit);
        });
    }

    BoletoEmail(context, codAcordo, codCarteira, email) {
        let dadosReq = {
            codAcordo: codAcordo,
            codCarteira: codCarteira,
            email: email
        };
        let varnames = new Array();

        if (!dadosReq.codAcordo)
            varnames.push("HASH(DADOS_CLIENTE,COD_ACORDO)");
        if (!dadosReq.codCarteira)
            varnames.push("HASH(DADOS_CLIENTE,COD_CARTEIRA)");
        if (!dadosReq.email)
            varnames.push("HASH(DADOS_CLIENTE,EMAIL_MAILING)");

        return this.service.constructor.getVariableMulti(context, varnames).then((varvalues) => {
            varvalues.forEach((variable) => {
                switch (variable.name) {
                case "HASH(DADOS_CLIENTE,COD_ACORDO)":      dadosReq.codAcordo = variable.value;    break;
                case "HASH(DADOS_CLIENTE,COD_CARTEIRA)":    dadosReq.codCarteira = variable.value;  break;
                case "HASH(DADOS_CLIENTE,EMAIL_MAILING)":   dadosReq.email = variable.value;        break;
                }
            });
            return this.deps[PASCHOALOTTO_WS_SERVICE].enviarBoletoViaEmail(context, dadosReq).then(result => {
                return context.setVariable("HASH(DADOS_CLIENTE,BOLETO_ENVIADO)", "1");
            });
        });
    }

    BoletoSms(context, codAcordo, codCarteira, numeroSMS) {
        let dadosReq = {
            codAcordo: codAcordo,
            codCarteira: codCarteira,
            numCelular: numeroSMS
        };
        let varnames = new Array();

        if (!dadosReq.codAcordo)
            varnames.push("HASH(DADOS_CLIENTE,COD_ACORDO)");
        if (!dadosReq.codCarteira)
            varnames.push("HASH(DADOS_CLIENTE,COD_CARTEIRA)");
        if (!dadosReq.numCelular)
            varnames.push("NUMERO_SMS");

        return this.service.constructor.getVariableMulti(context, varnames).then((varvalues) => {
            varvalues.forEach((variable) => {
                switch (variable.name) {
                case "HASH(DADOS_CLIENTE,COD_ACORDO)":      dadosReq.codAcordo = variable.value;    break;
                case "HASH(DADOS_CLIENTE,COD_CARTEIRA)":    dadosReq.codCarteira = variable.value;  break;
                case "NUMERO_SMS":                          dadosReq.numCelular = variable.value;   break;
                }
            });
            return this.deps[PASCHOALOTTO_WS_SERVICE].enviarBoletoViaSms(context, dadosReq).then(result => {
                return context.setVariable("HASH(DADOS_CLIENTE,BOLETO_ENVIADO)", "1");
            });
        });
    }

    CriarAcionamento(context, codCarteira, cpfCnpj, contrato,ddd ,telefone,dataAcionamento,horaAcionamento,codAcionamento,historico,tipoAcionamento,tempoLigacao) {
        let dadosReq = {
            codCarteira: codCarteira,
            cpfCnpj: cpfCnpj,
            contrato: contrato,
            ddd: ddd,
            telefone: telefone,
            dataAcionamento: dataAcionamento,
            horaAcionamento: horaAcionamento,
            codAcionamento: codAcionamento,
            historico: historico,
            tipoAcionamento: tipoAcionamento,
            tempoLigacao:  tempoLigacao
        };
        let varnames = new Array();

        if (!dadosReq.codCarteira)
            varnames.push("HASH(DADOS_CLIENTE,COD_CARTEIRA)");
        if (!dadosReq.cpfCnpj)
            varnames.push("HASH(DADOS_CLIENTE,CPF)");
        if (!dadosReq.contrato)
            varnames.push("HASH(DADOS_CLIENTE,CONTRATO)");
        if (!dadosReq.ddd)
            varnames.push("DDD");
        if (!dadosReq.telefone)
            varnames.push("TELEFONE");
        if (!dadosReq.dataAcionamento){
            let dt = new Date();
            dadosReq.dataAcionamento =dt.toISOString().slice(0,10);
        }
        if (!dadosReq.horaAcionamento){
            let dt = new Date();
            dadosReq.horaAcionamento =dt.toLocaleTimeString();
        }
        if (!dadosReq.codAcionamento)
            dadosReq.codAcionamento =0;
        if (!dadosReq.historico)
            dadosReq.codAcionamento =0;
        if (!dadosReq.tipoAcionamento)
            dadosReq.tipoAcionamento =2;
        if (!dadosReq.tempoLigacao)
            dadosReq.tempoLigacao =0;

        return this.service.constructor.getVariableMulti(context, varnames).then((varvalues) => {
            varvalues.forEach((variable) => {
                switch (variable.name) {
                    case "HASH(DADOS_CLIENTE,COD_CARTEIRA)":      dadosReq.codCarteira = variable.value;    break;
                    case "HASH(DADOS_CLIENTE,CPF)":    dadosReq.cpfCnpj = variable.value;  break;
                    case "HASH(DADOS_CLIENTE,CONTRATO)":   dadosReq.contrato = variable.value;        break;
                    case "DDD":   dadosReq.ddd = variable.value;        break;
                    case "TELEFONE":   dadosReq.telefone = variable.value;        break;
                }
            });
		console.log(horaAcionamento);
            return this.deps[PASCHOALOTTO_WS_SERVICE].criarAcionamento(context, dadosReq).then(result => {
                return context.setVariable("ACIONAMENTO_POSTADO", "1");
            });
        });
    }
}
