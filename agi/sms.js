
module.exports = class {
    init(service, config) {
        this.service = service;
    }

    sms(context, telefone, mensagem, service) {
        return (telefone ? Promise.resolve({telefone: telefone}) : this.service.constructor.getVariable(context, "NUMERO_SMS").then(value => {
            if (!value)
                throw new Error("Variavel NUMERO_SMS nao setada");
            return {telefone: value};
        })).then(sms => {
            if (mensagem) {
                sms.mensagem = mensagem;
                return sms;
            }
            return this.service.constructor.getVariable(context, "MENSAGEM_SMS").then(value => {
                if (!value)
                    throw new Error("Variavel MENSAGEM_SMS nao setada");
                sms.mensagem = value;
                return sms;
            });
        }).then(sms => {
            if (service) {
                sms.service = service;
                return sms;
            }
            return this.service.constructor.getVariable(context, "SERVICE_SMS").then(value => {
                if (!value)
                    throw new Error("Variavel SERVICE_SMS nao setada");
                sms.service = value;
                return sms;
            });
        }).then(sms => this.service.getDep("sms-" + sms.service).then(mod => {
                sms.service = mod;
                return sms;
            })
        ).then(sms => sms.service.sendMessage(context, sms.telefone, sms.mensagem)).then(result => context.setVariable("STATUS_SMS", result.result));
    }
}
