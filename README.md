# optimus-agi

## multicobra

#### Configuração

config.json:

```
{
  "deps": {
    "multicobra-ws": {
      "host": "localhost",
      "port": "5006",
      "ssl": false
    }
  }
}
```

#### Métodos

### multicobra/ValidarDevedor(cpf)

```
Sended:  agi_channel:SIP/0004F2060EB4-00000009
Sended:  agi_network_script:multicobra/ValidarDevedor
Sended:  agi_arg_1:01439279730
Sended:
Received:  GET VARIABLE AGI_SOUND
Sended:  200 result=1 (sound-file-placeholder)
Received:  STREAM FILE "sound-file-placeholder" ""
Sended:  200 result=1
Received:  SET VARIABLE "HASH(DADOS_CLIENTE,CODIGO)" 951138751
Sended:  200 result=1
```

### multicobra/ConsultarDividasAtivas(cpf)

```
Sended:  agi_channel:SIP/0004F2060EB4-00000009
Sended:  agi_network_script:multicobra/ConsultarDividasAtivas
Sended:  agi_arg_1:01439279730
Sended:
Received:  GET VARIABLE "WEB_SQL_INSERT_LOG_REQUEST_V1(0,localhost:5006,/v1/consultarDividasAtivas,POST,application/json,%7B%22Identificador%22%3A%22014
39279730%22%7D)"
Sended:  200 result=1 (666)
Received:  GET VARIABLE AGI_SOUND
Sended:  200 result=1 (sound-file-placeholder)
Received:  STREAM FILE "sound-file-placeholder" ""
Sended:  200 result=1
Received:  SET VARIABLE "WEB_SQL_INSERT_LOG_RESPONSE_V1(666,200,application/json; charset=utf-8)" "%5B%7B%22AnoFabricacao%22%3A%222012%22%2C%22Cliente%2
2%3A%22Finasa%22%2C%22ClienteCodigo%22%3A1537%2C%22ContratoCodigo%22%3A%22137651263%22%2C%22ContratoNumero%22%3A%2209.6.666.572-8%22%2C%22DevedorCodigo%
22%3A951138751%2C%22Filial%22%3A%221%22%2C%22FormaSelecaoParcela%22%3A%22QualquerContrato%22%2C%22Integracao%22%3A%225%22%2C%22Placas%22%3A%22KOX1784%22
%2C%22SaldoAtualizadoTotal%22%3A%222095.6100000000%22%7D%5D"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "HASH(DIVIDA,COUNT)" 1
Sended:  200 result=1
Received:  SET VARIABLE "HASH(DIVIDA,0_CONTRATO_NUMERO)" "09.6.666.572-8"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(DIVIDA,0_INTEGRACAO)" 5
Sended:  200 result=1
Received:  SET VARIABLE "HASH(DIVIDA,0_FILIAL)" 1
Sended:  200 result=1
Received:  SET VARIABLE "HASH(DIVIDA,0_DEVEDOR_CODIGO)" 951138751
Sended:  200 result=1
Received:  SET VARIABLE "HASH(DIVIDA,0_SALDO)" "2095.6100000000"
Sended:  200 result=1
```

### multicobra/ConsultarParcelasNegociaveis(integracao, devedorCodigo, filial, vencimento, contrato)

```
Sended:  agi_channel:SIP/0004F2060EB4-00000009
Sended:  agi_network_script:multicobra/ConsultarParcelasNegociaveis
Sended:  agi_arg_1:5
Sended:  agi_arg_2:951138751
Sended:  agi_arg_3:1
Sended:  agi_arg_4:2017-10-27
Sended:  agi_arg_5:09.6.666.572-8
Sended:
Received:  GET VARIABLE "WEB_SQL_INSERT_LOG_REQUEST_V1(0,localhost:5006,/v1/consultarParcelasNegociaveis,POST,application/json,%7B%22Integracao%22%3A%22
5%22%2C%22DevedorCodigo%22%3A%22951138751%22%2C%22Filial%22%3A%221%22%2C%22Vencimento%22%3A%222017-10-27%22%2C%22Contratos%22%3A%5B%2209.6.666.572-8%22%
5D%7D)"
Sended:  200 result=1 (666)
Received:  GET VARIABLE AGI_SOUND
Sended:  200 result=1 (sound-file-placeholder)
Received:  STREAM FILE "sound-file-placeholder" ""
Sended:  200 result=1
Received:  GET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(%5B%7B%22CodigoErro%22%3A0)"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(666)" "%2C%22Erro%22%3Afalse%2C%22Mensagem%22%3A%22%22%2C%22BoletoSobre%22%3A%22Parcela%22
%2C%22ChaveControleNegociacao%22%3A%22464b8740-9e45-49cb-8f7e-ab6e459bf2d2%22%2C%22Cliente%22%3A1537%2C%22ContratoCodigo%22%3A%22137651263%22%2C%22Contr
atoNumero%22%3A%2209.6.666.572-8%22%2C%22ContratoPlano%22%3A48%2C%22Fase%22%3A1%2C%22Indexador%22%3A%2211%22%2C%22MaiorVencimento%22%3A%2230%2F04%2F2019
%22%2C%22ParCalculo%22%3A41762%2C%22ParCalculoItem%22%3A18%2C%22ParCalculoItemPc%22%3A1%2C%22ParcelaAtrasoCalculo%22%3A61%2C%22ParcelaCodigo%22%3A6%2C%2
2ParcelaComissaoPermanencia%22%3A%220.0000%22%2C%22ParcelaComissaoPermanenciaOriginal%22%3A%220.0000%22%2C%22ParcelaCorrecaoMonetaria%22%3A%220.0000%22%
2C%22Parc"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(666)" "elaCustas%22%3A%220.0000%22%2C%22ParcelaHonorarios%22%3A%2295.0000%22%2C%22ParcelaH
onorariosOriginal%22%3A%2296.8000%22%2C%22ParcelaJuros%22%3A%2250.8800%22%2C%22ParcelaJurosOriginal%22%3A%2250.8800%22%2C%22ParcelaMulta%22%3A%220.0000%
22%2C%22ParcelaMultaOriginal%22%3A%2217.9800%22%2C%22ParcelaNegociacaoObrigatoria%22%3Atrue%2C%22ParcelaNegociavel%22%3Atrue%2C%22ParcelaNotificacao%22%
3A%220.0000%22%2C%22ParcelaNumero%22%3A28%2C%22ParcelaSaldoAtualizado%22%3A%221045.0000%22%2C%22ParcelaSaldoAtualizadoOriginal%22%3A%221064.7800%22%2C%2
2ParcelaSituacao%22%3A%22Em%20Atraso%22%2C%22ParcelaTarifa%22%3A%220.0000%22%2C%22ParcelaValor%22%3A%22899.1200%22%2C%22ParcelaValorDesconto%22%3A%22"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(666)" "899.1200%22%2C%22ParcelaVencimento%22%3A%2230%2F08%2F2017%22%2C%22ParcelaVencimento
ParCalculo%22%3A%2230%2F10%2F2017%22%2C%22PermitirParcelaPulada%22%3Afalse%2C%22QtdParcelas%22%3A21%7D%2C%7B%22CodigoErro%22%3A0%2C%22Erro%22%3Afalse%2C
%22Mensagem%22%3A%22%22%2C%22BoletoSobre%22%3A%22Parcela%22%2C%22ChaveControleNegociacao%22%3A%22464b8740-9e45-49cb-8f7e-ab6e459bf2d2%22%2C%22Cliente%22
%3A1537%2C%22ContratoCodigo%22%3A%22137651263%22%2C%22ContratoNumero%22%3A%2209.6.666.572-8%22%2C%22ContratoPlano%22%3A48%2C%22Fase%22%3A1%2C%22Indexado
r%22%3A%2211%22%2C%22MaiorVencimento%22%3A%2230%2F04%2F2019%22%2C%22ParCalculo%22%3A41762%2C%22ParCalculoItem%22%3A18%2C%22ParCalculoItemPc%22%3A1%2C%22
ParcelaAtrasoCalcul"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(666)" "o%22%3A30%2C%22ParcelaCodigo%22%3A7%2C%22ParcelaComissaoPermanencia%22%3A%220.0000%
22%2C%22ParcelaComissaoPermanenciaOriginal%22%3A%220.0000%22%2C%22ParcelaCorrecaoMonetaria%22%3A%220.0000%22%2C%22ParcelaCustas%22%3A%220.0000%22%2C%22P
arcelaHonorarios%22%3A%2292.4100%22%2C%22ParcelaHonorariosOriginal%22%3A%2294.2100%22%2C%22ParcelaJuros%22%3A%2225.0200%22%2C%22ParcelaJurosOriginal%22%
3A%2225.0200%22%2C%22ParcelaMulta%22%3A%220.0000%22%2C%22ParcelaMultaOriginal%22%3A%2217.9800%22%2C%22ParcelaNegociacaoObrigatoria%22%3Afalse%2C%22Parce
laNegociavel%22%3Atrue%2C%22ParcelaNotificacao%22%3A%220.0000%22%2C%22ParcelaNumero%22%3A29%2C%22ParcelaSaldoAtualizado%22%3A%221016.5500%22%2C"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_END_CHUNK_LOG_RESPONSE_V1(666,666,200,application/json; charset=utf-8)" "%22ParcelaSaldoAtualizadoOriginal%22%3A
%221036.3300%22%2C%22ParcelaSituacao%22%3A%22Em%20Atraso%22%2C%22ParcelaTarifa%22%3A%220.0000%22%2C%22ParcelaValor%22%3A%22899.1200%22%2C%22ParcelaValor
Desconto%22%3A%22899.1200%22%2C%22ParcelaVencimento%22%3A%2230%2F09%2F2017%22%2C%22ParcelaVencimentoParCalculo%22%3A%2230%2F10%2F2017%22%2C%22PermitirPa
rcelaPulada%22%3Afalse%2C%22QtdParcelas%22%3A21%7D%5D"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "HASH(PARCELA,COUNT)" 2
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_CONTRATO_NUMERO)" "09.6.666.572-8"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_PARCELA_NUMERO)" 28
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_PARCELA_VENCIMENTO)" "30/08/2017"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_CHAVE_CONTROLE_NEGOCIACAO)" "464b8740-9e45-49cb-8f7e-ab6e459bf2d2"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_CONTRATO_CODIGO)" 137651263
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_PARCELA_CODIGO)" 6
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_CONTRATO_PARCELA_CODIGO)" "137651263-6"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_PARCELA_SITUACAO)" "Em Atraso"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_PARCELA_ATRASO_CALCULO)" 61
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_PARCELA_VALOR)" "899.1200"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_PARCELA_VALOR_DESCONTO)" "899.1200"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_PARCELA_SALDO_ATUALIZADO_ORIGINAL)" "1064.7800"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,0_PARCELA_SALDO_ATUALIZADO)" "1045.0000"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_CONTRATO_NUMERO)" "09.6.666.572-8"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_PARCELA_NUMERO)" 29
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_PARCELA_VENCIMENTO)" "30/09/2017"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_CHAVE_CONTROLE_NEGOCIACAO)" "464b8740-9e45-49cb-8f7e-ab6e459bf2d2"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_CONTRATO_CODIGO)" 137651263
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_PARCELA_CODIGO)" 7
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_CONTRATO_PARCELA_CODIGO)" "137651263-7"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_PARCELA_SITUACAO)" "Em Atraso"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_PARCELA_ATRASO_CALCULO)" 30
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_PARCELA_VALOR)" "899.1200"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_PARCELA_VALOR_DESCONTO)" "899.1200"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_PARCELA_SALDO_ATUALIZADO_ORIGINAL)" "1036.3300"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(PARCELA,1_PARCELA_SALDO_ATUALIZADO)" "1016.5500"
Sended:  200 result=1
```

### multicobra/ListarParcelamentos(chaveControleNegociacao, vencimento, entrada, parcelado, contratoParcela)

```
Sended:  agi_channel:SIP/0004F2060EB4-00000009
Sended:  agi_network_script:multicobra/ListarParcelamentos
Sended:  agi_arg_1:464b8740-9e45-49cb-8f7e-ab6e459bf2d2
Sended:  agi_arg_2:2017-10-27
Sended:  agi_arg_3:0
Sended:  agi_arg_4:0
Sended:  agi_arg_5:137651263-6,137651263-7
Sended:
Received:  GET VARIABLE "WEB_SQL_INSERT_LOG_REQUEST_V1(0,localhost:5006,/v1/listarParcelamentos,POST,application/json,%7B%22ChaveControleNegociacao%22%3
A%22464b8740-9e45-49cb-8f7e-ab6e459bf2d2%22%2C%22Data%22%3A%222017-10-27%22%2C%22Entrada%22%3A%220%22%2C%22Parcelado%22%3Afalse%2C%22ContratosParcelas%2
2%3A%5B%7B%22ContratoCodigo%22%3A%22137651263%22%2C%22ParcelaCodigo%22%3A%226%22%7D%2C%7B%22ContratoCodigo%22%3A%22137651263%22%2C%22ParcelaCodigo%22%3A
%227%22%7D%5D%7D)"
Sended:  200 result=1 (666)
Received:  GET VARIABLE AGI_SOUND
Sended:  200 result=1 (sound-file-placeholder)
Received:  STREAM FILE "sound-file-placeholder" ""
Sended:  200 result=1
Received:  SET VARIABLE "WEB_SQL_INSERT_LOG_RESPONSE_V1(666,200,application/json; charset=utf-8)" "%5B%7B%22CodigoErro%22%3A0%2C%22Erro%22%3Afalse%2C%22
ChaveControleNegociacao%22%3A%22464b8740-9e45-49cb-8f7e-ab6e459bf2d2%22%2C%22Codigo%22%3A%221%22%2C%22DataBase%22%3A%222017-10-27T12%3A20%3A13.783Z%22%2
C%22DataVencimento%22%3A%222017-10-30T00%3A00%3A00.000Z%22%2C%22Plano%22%3A%221%22%2C%22Saldo%22%3A%222061.55%22%2C%22SaldoOriginal%22%3A%222101.11%22%2
C%22Valor%22%3A%222061.55%22%2C%22ValorBase%22%3A%222061.55%22%2C%22ValorDesconto%22%3A%2239.56%22%2C%22ValorEntrada%22%3A%222061.55%22%2C%22ValorEntrad
aMinimo%22%3A%220%22%2C%22ValorOriginal%22%3A%222101.11%22%2C%22ValorParcela%22%3A%220%22%7D%5D"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "HASH(NEGOCIACAO,COUNT)" 1
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_CHAVE_CONTROLE_NEGOCIACAO)" "464b8740-9e45-49cb-8f7e-ab6e459bf2d2"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_CODIGO_PARCELAMENTO)" 1
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_DATA_BASE)" "2017-10-27T12:20:13.783Z"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_DATA_VENCIMENTO)" "2017-10-30T00:00:00.000Z"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_PLANO)" 1
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_SALDO)" "2061.55"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_SALDO_ORIGINAL)" "2101.11"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_VALOR)" "2061.55"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_VALOR_BASE)" "2061.55"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_VALOR_DESCONTO)" "39.56"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_VALOR_ENTRADA)" "2061.55"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_VALOR_ENTRADA_MINIMO)" 0
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_VALOR_ORIGINAL)" "2101.11"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(NEGOCIACAO,0_VALOR_PARCELA)" 0
Sended:  200 result=1
```

### multicobra/SalvarNegociacao(chaveControleNegociacao, codigoParcelamento[, email, ddd, telefone])

```
Sended:  agi_channel:SIP/0004F2060EB4-00000009
Sended:  agi_network_script:multicobra/SalvarNegociacao
Sended:  agi_arg_1:464b8740-9e45-49cb-8f7e-ab6e459bf2d2
Sended:  agi_arg_2:1
Sended:
Received:  GET VARIABLE "HASH(DADOS_CLIENTE,EMAIL)"
Sended:  200 result=0
Received:  GET VARIABLE "HASH(DADOS_CLIENTE,DDD)"
Sended:  200 result=0
Received:  GET VARIABLE "HASH(DADOS_CLIENTE,TELEFONE)"
Sended:  200 result=0
Received:  GET VARIABLE "WEB_SQL_INSERT_LOG_REQUEST_V1(0,localhost:5006,/v1/salvarNegociacao,POST,application/json,%7B%22ChaveControleNegociacao%22%3A%2
2464b8740-9e45-49cb-8f7e-ab6e459bf2d2%22%2C%22CodigoParcelamento%22%3A%221%22%7D)"
Sended:  200 result=1 (666)
Received:  GET VARIABLE AGI_SOUND
Sended:  200 result=1 (sound-file-placeholder)
Received:  STREAM FILE "sound-file-placeholder" ""
Sended:  200 result=1
Received:  GET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(%7B%22CodigoErro%22%3A0)"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(666)" "%2C%22Erro%22%3Afalse%2C%22Aceite%22%3A%22N%22%2C%22AcordoPlano%22%3A1%2C%22Agencia
%22%3A%222856%22%2C%22BairroCredor%22%3A%22Vila%20Yara%22%2C%22BairroDevedor%22%3A%22Pechincha%22%2C%22Banco%22%3A%22237%22%2C%22BoletoVencimento%22%3A%
2230%2F10%2F2017%22%2C%22CEPCedente%22%3A6029900%2C%22CEPCredor%22%3A6029900%2C%22CEPDevedor%22%3A22740300%2C%22CNPJCedente%22%3A%227207996000150%22%2C%
22CNPJCredor%22%3A%227207996000150%22%2C%22CPF_CNPJDevedor%22%3A%221439279730%22%2C%22Carteira%22%3A%2209%22%2C%22CidadeCredor%22%3A%22Osasco%22%2C%22Ci
dadeDevedor%22%3A%22Rio%20de%20Janeiro%22%2C%22ClienteCodigo%22%3A1537%2C%22CodigoBarras%22%3A%2223795732800002061552856091798041453904000010%22%2C%22Co
digoBoleto%22%3A%2225037385%22%2C%22Con"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(666)" "ta%22%3A%22400001%22%2C%22ContratoNumero%22%3A%2209.6.666.572-8%3B%22%2C%22Contrato
Plano%22%3A%2248%22%2C%22Custas%22%3A%220.0000%22%2C%22Desconto%22%3A%221798.2400%22%2C%22DigitoBanco%22%3A%222%22%2C%22DigitoConta%22%3A%223%22%2C%22Di
gitoNossoNumero%22%3A%22P%22%2C%22EnderecoCedente%22%3A%22Av.Cidade%20de%20Deus%2C%20S%2FN%20-%20P.Prata%202%C2%BAAndar%22%2C%22EnderecoCredor%22%3A%22A
v.Cidade%20de%20Deus%2C%20S%2FN%20-%20P.Prata%202%C2%BAAndar%22%2C%22EnderecoDevedor%22%3A%22R%20Ana%20Silva%22%2C%22Especie%22%3A%22R%24%22%2C%22Especi
eDocumento%22%3A%22OU%22%2C%22EstadoDevedor%22%3A%22RJ%22%2C%22Filial%22%3A1%2C%22Honorarios%22%3A%22187.4100%22%2C%22InstrucaoBoleto%22%3A%22****N%C3%A
3o%20Receber%20ap%C3%B3s%20o%20vencto****%20N%C3%A3o%20receber%20valor%20diferente%20d"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(666)" "o%20documento!%5Cr%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5CtEmpresa%20Mandat%C3%A1ria%3A%20Multic
obra%20Fone%3A%200800%20979-4103%20Rua%20Gustavo%20Maciel%2C%2012-33%2C%20Centro%2C%20Bauru-SP%2C%20CEP%2017015320%2C%20CPF%3A%201439279730%20Pagto%20Co
ntrato(s)%3A%2009.6.666.572-8%3B%2C%20Referente%20aos%20Vencimentos%20listados%20acima%5Cr%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5CtSr.%20Caixa%2C%20%C3%A9%20obrigat%
C3%B3rio%20o%20cadastramento%20correto%20dos%20dados%2C%20com%20todos%20os%20algarismos%20da%20linha%20digit%C3%A1vel%2C%5Cr%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ct
caso%20a%20leitura%20%C3%B3tica%20n%C3%A3o%20seja%20poss%C3%ADvel.%20O%20prazo%20para%20retirada%20de%20restri%C3%A7%C3%B5es%20(se%20houver)%20junto%20a
o%20SCPC%20e%2Fou%20Serasa%5Cr%5Cn%5Ct%5Ct%5Ct%5Ct%5Ct%5Ctser%C3%A1%20d"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(666)" "e%20at%C3%A9%2010%20dias%20%C3%BAteis%20ap%C3%B3s%20a%20data%20do%20pagamento.%22%2
C%22Juros%22%3A%2275.9000%22%2C%22LinhaDigitavel%22%3A%2223792.85600%2091798.041454%2039040.000109%205%2073280000206155%22%2C%22LocalPagamento%22%3A%22P
AGAVEL%20PREFERENCIALMENTE%20NAS%20AGENCIAS%20BRADESCO%22%2C%22Multa%22%3A%220.0000%22%2C%22NomeCedente%22%3A%22Banco%20Bradesco%20Financiamentos%20S.A.
%22%2C%22NomeCredor%22%3A%22Banco%20Bradesco%20Financiamentos%20S.A.%22%2C%22NomeSacado%22%3A%22Andrea%20de%20Freitas%20Mendonca%22%2C%22NossoNumero%22%
3A%2217980414539%22%2C%22Parcelas%22%3A%2228%3B29%3B%22%2C%22SaldoPendente%22%3A%220.0000%22%2C%22UFCredor%22%3A%22SP%22%2C%22Valor%22%3A%222061.5500%22
%2C%22ValorAtualizado%22%3A%221798.2"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "WEB_SQL_INSERT_END_CHUNK_LOG_RESPONSE_V1(666,666,200,application/json; charset=utf-8)" "400%22%2C%22ValorOriginal%22%3A%222101.
1100%22%2C%22Vencimentos%22%3A%2230%2F08%2F2017%3B30%2F09%2F2017%3B%22%7D"
Sended:  200 result=1 (666)
Received:  SET VARIABLE "HASH(DADOS_CLIENTE,BOLETO)" "23792.85600 91798.041454 39040.000109 5 73280000206155"
Sended:  200 result=1
Received:  SET VARIABLE "HASH(DADOS_CLIENTE,CODIGO_BOLETO)" 25037385
Sended:  200 result=1
```
