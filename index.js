
const fs = require("fs");
const process = require("process");
const agi = require("@thiagodk/agi");
const log4js = require("log4js");

var defaultConfig = {
    agi: {
        port: 4573,
        host: "127.0.0.1"
    },
    modules: {},
    deps: {},
    logger: {
        appenders: {
            stdout: {
                type: "stdout",
                layout: {type: "coloured"}
            },
            file: {
                type: "file",
                filename: "optimus-agi.log",
                layout: {type: "basic"}
            }
        },
        categories: {
            default: {
                appenders: ["stdout", "file"],
                level: "info"
            }
        }
    },
    pidfile: "optimus-agi.pid"
}

class OptimusAGI {
    constructor() {
        this.config = null;
        this.agi = null;
        this.logger = null;
        this.modules = {};
        this.deps = {};
    }

    loadConfig(configFile = "config.json") {
        this.config = Object.assign({}, defaultConfig);
        return new Promise((resolve, reject) => {
            fs.readFile(configFile, "utf8", (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    try {
                        resolve(Object.assign(this.config, JSON.parse(data)));
                    } catch (parseErr) {
                        reject(parseErr);
                    }
                }
            });
        });
    }

    start() {
        log4js.configure(this.config.logger);
        this.logger = log4js.getLogger("main");
        if (this.config.pidfile)
            fs.writeFile(this.config.pidfile, process.pid, (err) => { if (err) throw err; });
        this.agi = agi.createServer(this.processRequest.bind(this)).listen(this.config.agi.port, this.config.agi.host);
    }

    processRequest(context) {
        var scriptEndpointPath;
        var channelName;
        var endFunc = function() {
            this.logger.debug("AGI desconectado");
            if (Array.isArray(scriptEndpointPath) && typeof channelName == "string")
                this.logger.info("Modulo AGI[%s], Channel[%s]. Finalizado", scriptEndpointPath[0], channelName);
            context.end();
        }.bind(this);
        this.logger.debug("AGI conectado");
        (new Promise((resolve, reject) => {
            // Cria promessa inicial retornando as variaveis de inicialização da AGI
            if (Object.keys(context.variables).length)
                resolve(context.variables);
            else // Se as variaveis não existiverem carregadas...
                context.once("variables", resolve); // ... espera evento de 'variables' indicando que a variável foi corregada
        })).then((vars) => {
            // Busca módulo requisitado pela FastAGI
            this.logger.debug("Variaveis AGI inicializada: %s", JSON.stringify(vars, null, 2));
            scriptEndpointPath = vars.agi_network_script.split("/");
            channelName = vars.agi_channel;
            if (scriptEndpointPath.length < 2)
                throw new Error("Incomplete endpoint path");
            this.logger.info("Chamada da AGI: Channel[%s] Modulo[%s] Metodo[%s]", channelName, scriptEndpointPath[0], scriptEndpointPath[1]);
            if (typeof this.modules[scriptEndpointPath[0]] === "undefined") { // Módulo ainda não carregado
                this.logger.info("Modulo AGI[%s] não carregado, criando nova instancia do para o modulo solicitado", scriptEndpointPath[0]);
                let moduleAGI = require("./agi/" + scriptEndpointPath[0]);
                let config = typeof this.config.modules[scriptEndpointPath[0]] == "object" ? this.config.modules[scriptEndpointPath[0]] : {};
                this.modules[scriptEndpointPath[0]] = new moduleAGI();
                this.modules[scriptEndpointPath[0]].logger = log4js.getLogger("mod-" + scriptEndpointPath[0]);
                this.logger.debug("Config do Modulo AGI[%s]: %s", scriptEndpointPath[0], JSON.stringify(config, null, 2));
                if (typeof this.modules[scriptEndpointPath[0]].init == "function") {
                    this.logger.info("Inicializando modulo AGI[%s]", scriptEndpointPath[0]);
                    return Promise.resolve(this.modules[scriptEndpointPath[0]].init(this, config)).then(() => {
                        this.logger.info("Processo de inicializacao do modulo AGI[%s] concluido", scriptEndpointPath[0]);
                        return this.modules[scriptEndpointPath[0]]
                    });
                } else {
                    this.logger.warn("Modulo AGI[%s] nao possui processo de inicializacao. Pronto para uso!", scriptEndpointPath[0]);
                }
            }
            return Promise.resolve(this.modules[scriptEndpointPath[0]]);
        }).then((module) => {
            // Carrega dependencias
            if (!module.deps)
                return [module];

            let deps = module.deps instanceof Array ? module.deps : [module.deps];

            return Promise.all([Promise.resolve(module)].concat(deps.filter((dep) => {
                return typeof dep != "object";
            }).map((dep) => {
                this.logger.info("Carregando Dependencia[%s] para Modulo AGI[%s]", dep, scriptEndpointPath[0]);
                return this.getDep(dep).then((depObj) => {
                    return {name: dep, obj: depObj};
                });
            })));
        }).then((deps) => {
            // Seta as dependências carregadas no objeto do módulo
            let module = deps.shift();
            deps.forEach((dep) => {
                if (module.deps instanceof Array) {
                    for (let i = 0; i < module.deps.length; i++) {
                        if (typeof module.deps[i] != "object" && module.deps[i] == dep.name) {
                            module.deps[i] = dep.obj;
                            break;
                        }
                    }
                } else if (typeof module.deps != "object" && module.deps == dep.name) {
                    module.deps = dep.obj;
                }
            });
            return module;
        }).then((module) => {
            // Roda método do objeto do módulo
            let funcName = scriptEndpointPath[1]; // Nome do método a executar
            let funcArgs = [context];
            for (let i = 1; typeof context.variables["agi_arg_" + i] !== "undefined"; i++)
                funcArgs.push(context.variables["agi_arg_" + i]); // Parsing dos argumentos para ser passados ao método do módulo
            if (typeof module[funcName] != "function")
                throw new Error("Requested method not found");
            this.logger.info("Executando Modulo AGI[%s], Metodo[%s], Channel[%s], Parametros: %j", scriptEndpointPath[0], funcName, channelName, funcArgs.slice(1))
            return module[funcName].apply(module, funcArgs);
        }).then(endFunc).catch((err) => {
            this.logger.error("Erro na execução do Modulo AGI[%s], Channel[%s]: ", scriptEndpointPath[0], channelName, err);
            context.verbose("AGI ERROR: " + err, 2).then(endFunc).catch(endFunc);
        });
    }

    getDep(depName, loading = []) {
        this.logger.debug("Dependencia[%s] Solicitada", depName);
        if (loading.indexOf(depName) >= 0)
            return Promise.reject(new Error("Circular dependency detected"));
        loading.push(depName);
        if (typeof this.deps[depName] != "undefined") {
            return Promise.resolve(this.deps[depName]);
        } else {
            this.logger.info("Carregando Dependencia[%s]", depName);
            return (new Promise((resolve, reject) => {
                let moduleDep = require("./agi-depends/" + depName);
                let config = typeof this.config.deps[depName] == "object" ? this.config.deps[depName] : {};
                this.deps[depName] = new moduleDep();
                this.deps[depName].logger = log4js.getLogger("dep-" + depName);
                this.logger.debug("Config da Dependencia[%s]: %s", depName, JSON.stringify(config, null, 2));
                Promise.all((this.deps[depName].deps instanceof Array ? this.deps[depName].deps :
                        (this.deps[depName].deps ? [this.deps[depName].deps] : [])).filter(
                            dep => typeof dep != "object"
                        ).map(dep => {
                            this.logger.info("Carregando Dependencia[%s] para Dependencia[%s]", dep, depName);
                            return this.getDep(dep, loading).then(depObj => { return {name: dep, obj: depObj}});
                        })).then(deps => {
                    deps.forEach((dep) => {
                        if (this.deps[depName].deps instanceof Array) {
                            for (let i = 0; i < this.deps[depName].deps.length; i++) {
                                if (typeof this.deps[depName].deps[i] != "object" && this.deps[depName].deps[i] == dep.name) {
                                    this.deps[depName].deps[i] = dep.obj;
                                    break;
                                }
                            }
                        } else if (typeof this.deps[depName].deps != "object" && this.deps[depName].deps == dep.name) {
                            this.deps[depName].deps = dep.obj;
                        }
                    });
                    return this.deps[depName].deps;
                }).then(() => {
                    if (typeof this.deps[depName].init == "function") {
                        this.logger.info("Inicializando Dependencia[%s]", depName);
                        Promise.resolve(this.deps[depName].init(this, config)).then(() => {
                            this.logger.info("Processo de inicialização da Dependencia[%s] concluido", depName);
                            resolve(this.deps[depName]);
                        }).catch(reject);
                    } else {
                        this.logger.warn("Dependencia[%s] nao possui processo de inicializacao. Pronto para uso!", depName);
                        resolve(this.deps[depName]);
                    }
                });
            })).catch(err => {
                this.logger.error("Falha ao carregar Dependencia[%s]: %s", depName, err);
                delete this.deps[depName];
                return Promise.reject(err);
            });
        }
    }

    unloadModule(modName) {
        let modDelete = new Array(); // Array de modulos a serem excluidos do require.cache
        this.logger.info("Descarregando Modulo[%s]", modName);
        for (let modNameCache in require.cache)
            if (modNameCache.startsWith(__dirname + "/agi/" + modName + "/"))
                modDelete.push(modNameCache);
        modDelete.forEach(modNameCache => delete require.cache[modNameCache]);
        return delete this.modules[modName];
    }

    unloadDep(depName) {
        let modDelete = new Array(); // Array de modulos a serem excluidos do require.cache
        this.logger.info("Descarregando Dependencia[%s]", depName);
        for (let modNameCache in require.cache)
            if (modNameCache.startsWith(__dirname + "/agi-depends/" + depName + "/"))
                modDelete.push(modNameCache);
        modDelete.forEach(modNameCache => delete require.cache[modNameCache]);
        return delete this.deps[depName];
    }

    // Helper Methods
    static getVariable(context, variable) {
        return context.getVariable(variable).then(response => {
            if (response.code == 200) {
                let m = /^([01]) \((.*)\)/.exec(response.result);
                if (m && parseInt(m[1]))
                    return m[2];
            }
            return null;
        });
    }

    static getFullVariable(context, variable) {
        return context.getFullVariable(variable).then(response => {
            if (response.code == 200) {
                let m = /^([01]) \((.*)\)/.exec(response.result);
                if (m && parseInt(m[1]))
                    return m[2];
            }
            return null;
        });
    }

    static getVariableMulti(context, variables) {
        var results = new Array();
        var getPromise = function(name) {
            return function() {
                return context.getVariable(name).then((response) => {
                    if (response.code == 200) {
                        let m = /^([01]) \((.*)\)/.exec(response.result);
                        if (m && parseInt(m[1]))
                            return {name: name, value: m[2]};
                    }
                    return {name: name, value: null};
                }).then((varvalue) => {
                    results.push(varvalue);
                    return results;
                });
            };
        };
        var p = Promise.resolve(results);
        while (variables.length)
            p = p.then(getPromise(variables.shift()));
        return p;
    }

    static getFullVariableMulti(context, variables) {
        var results = new Array();
        var getPromise = function(name) {
            return function() {
                return context.getFullVariable(name).then((response) => {
                    if (response.code == 200) {
                        let m = /^([01]) \((.*)\)/.exec(response.result);
                        if (m && parseInt(m[1]))
                            return {name: name, value: m[2]};
                    }
                    return {name: name, value: null};
                }).then((varvalue) => {
                    results.push(varvalue);
                    return results;
                });
            };
        };
        var p = Promise.resolve(results);
        while (variables.length)
            p = p.then(getPromise(variables.shift()));
        return p;
    }

    static setVariableMulti(context, variables) {
        var results = new Array();
        var getPromise = function(varvalue) {
            return function() {
                return context.setVariable(varvalue.name, varvalue.value).then((response) => {
                    results.push({name: varvalue.name, value: varvalue.value, status: (response.code == 200 && parseInt(response.result))});
                    return results;
                });
            };
        };
        var p = Promise.resolve(results);
        while (variables.length)
            p = p.then(getPromise(variables.shift()));
        return p;
    }
}

module.exports = OptimusAGI;
