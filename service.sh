#!/bin/sh

SCRIPT_PATH=$(dirname "$0")
cd "$SCRIPT_PATH"
exec setsid node -e 'const OptimusAGI = require("./index"); var instance = new OptimusAGI(); instance.loadConfig().then((config) => { instance.start(); });' 1>agi-$(date +'%Y%m%d%H%M').log 2>agi-error-$(date +'%Y%m%d%H%M').log &
