#!/bin/sh

### BEGIN INIT INFO
# Provides:             optimus-agi
# Required-Start:       $network $syslog $local_fs $remote_fs
# Required-Stop:        $network $syslog $local_fs $remote_fs
# Should-Start:         asterisk dahdi
# Should-Stop:          asterisk dahdi
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    Optimus FastAGI service
# Description:          Optimus Dialer plug-n-play FastAGI service core
### END INIT INFO

# Source function library.
. /etc/rc.d/init.d/functions

AGI_BIN_PATH=/var/lib/asterisk/agi-bin

SERVICE_DIR=optimus-agi

SERVICE_BIN=service.sh

if [ -f /etc/sysconfig/optimus-agi ]; then
    . /etc/sysconfig/optimus-agi
fi

start() {
    echo -n $"Starting Optimus FastAGI Service: "
    daemon --pidfile="$AGI_BIN_PATH/$SERVICE_DIR/optimus-agi.pid" "$AGI_BIN_PATH/$SERVICE_DIR/$SERVICE_BIN"
    return $?
}

stop() {
    echo -n $"Stopping Optimus FastAGI Service: "
    killproc -p "$AGI_BIN_PATH/$SERVICE_DIR/optimus-agi.pid"
    return $?
}

restart() {
    stop
    start
}

case "$1" in
    start)
        start
        ;;

    stop)
        stop
        ;;

    restart)
        restart
        ;;

    status)
        status -p "$AGI_BIN_PATH/$SERVICE_DIR/optimus-agi.pid"
        ;;

    *)
        echo "Usage: optimus-agi {start|stop|restart|status}"
        exit 1
esac

exit $?
