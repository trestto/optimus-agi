
const HTTP_SERVICE = 0;

const util = require("util");
const http = require("http");
const https = require("https");

//----- START: Private Functions -----//


function httpRequest(context, options, postData, sendAuth = true) {
    options = Object.assign({headers: {}}, this.defaultOptions, options);
    if (sendAuth && this.token) options.headers["Token"] = this.token;
    return this.deps[HTTP_SERVICE].httpRequest(context, options, postData);
}

function tokenExceptionHandler(err, context = null) {
    if (err instanceof this.deps[HTTP_SERVICE].constructor.HttpError && err.code == 400 && err.data) {
      let response = JSON.parse(err.data);
      if (typeof response.ResponseStatus == "object" && response.ResponseStatus.ErrorCode == "TokenException") { // Token expirou
        this.logger.error("Token de autenticacao invalido. Tentando revalidar o token");
        return auth.call(this, context); // Refaz autenticação
      }
    }
    return Promise.reject(err);
}

function auth(context, usuario, senha) {
    if (usuario) this.usuario = usuario;
    if (senha) this.senha = senha;
    this.logger.info("WebRequest(autenticacao): usuario(%s), senha(%s)", this.usuario, this.senha);
    return httpRequest.call(this, context, {
        path: "/v1/autenticacao",
        method: "POST"
    }, {usuario: this.usuario, senha: this.senha}, false).then((result) => {
        this.logger.info("WebResponse(autenticacao): %s", result.response.BearerToken);
        return this.token = result.response.BearerToken;
    });
}

//----- END: Private Functions -----//

module.exports = class {
    constructor() {
        this.token = null;
        this.deps = ["http"];
    }

    init(service, config) {
        this.service = service;
        this.httpAgent = config.ssl ? new https.Agent({keepAlive: true}) : new http.Agent({keepAlive: true});
        this.defaultOptions = {
            hostname: config.host,
            port: config.port,
            agent: this.httpAgent,
            headers: {"Content-Type": "application/json"}
        };
        this.config = config;
        return auth.call(this, null, config.auth.user, config.auth.pass);
    }

    negociacaoOpcao(context, params) {
        if (!this.token) {
            return auth.call(this, context).then(() => this.negociacaoOpcao(context, params));
        } else {
            this.logger.info("WebRequest(negociacaoOpcao): %j", params);
            return httpRequest.call(this, context, {
                path: "/v1/negociacaoOpcao",
                method: "POST"
            }, params).then(result => {
                let isObj = (typeof result.response == "object");
                this.logger.info("WebResponse(negociacaoOpcao) %" + (isObj ? "j" : "s"), isObj ? result.response : "Not an object");
                return result;
            }).catch(err => {
                this.logger.error("WebResponse(negociacaoOpcao) Error %d", err.code);
                return tokenExceptionHandler.call(this, err, context).then(() => this.negociacaoOpcao(context, params))
            });
        }
    }

    criarAcordo(context, params) {
        if (!this.token) {
            return auth.call(this, context).then(() => this.criarAcordo(context, params));
        } else {
            this.logger.info("WebRequest(criarAcordo): %j", params);
            return httpRequest.call(this, context, {
                path: "/v1/criarAcordo",
                method: "POST"
            }, params).then(result => {
                let isObj = (typeof result.response == "object");
                this.logger.info("WebResponse(criarAcordo) %" + (isObj ? "j" : "s"), isObj ? result.response : "Not an object");
                return result;
            }).catch(err => {
                this.logger.error("WebResponse(criarAcordo) Error %d", err.code);
                return tokenExceptionHandler.call(this, err, context).then(() => this.criarAcordo(context, params))
            });
        }
    }

    criarBoleto(context, params) {
        if (!this.token) {
            return auth.call(this, context).then(() => this.criarBoleto(context, params));
        } else {
            this.logger.info("WebRequest(criarBoleto): %j", params);
            return httpRequest.call(this, context, {
                path: "/v1/criarBoleto",
                method: "POST"
            }, params).then(result => {
                let isObj = (typeof result.response == "object");
                this.logger.info("WebResponse(criarBoleto) %" + (isObj ? "j" : "s"), isObj ? result.response : "Not an object");
                return result;
            }).catch(err => {
                this.logger.error("WebResponse(criarBoleto) Error %d", err.code);
                return tokenExceptionHandler.call(this, err, context).then(() => this.criarBoleto(context, params))
            });
        }
    }

    enviarBoletoViaEmail(context, params) {
        if (!this.token) {
            return auth.call(this, context).then(() => this.enviarBoletoViaEmail(context, params));
        } else {
            this.logger.info("WebRequest(enviarBoletoViaEmail): %j", params);
            return httpRequest.call(this, context, {
                path: "/v1/enviarBoletoViaEmail",
                method: "POST"
            }, params).then(result => {
                let isObj = (typeof result.response == "object");
                this.logger.info("WebResponse(enviarBoletoViaEmail) %" + (isObj ? "j" : "s"), isObj ? result.response : "Not an object");
                return result;
            }).catch(err => {
                this.logger.error("WebResponse(enviarBoletoViaEmail) Error %d", err.code);
                return tokenExceptionHandler.call(this, err, context).then(() => this.enviarBoletoViaEmail(context, params))
            });
        }
    }

    enviarBoletoViaSms(context, params) {
        if (!this.token) {
            return auth.call(this, context).then(() => this.enviarBoletoViaSms(context, params));
        } else {
            this.logger.info("WebRequest(enviarBoletoViaSms): %j", params);
            return httpRequest.call(this, context, {
                path: "/v1/enviarBoletoViaSms",
                method: "POST"
            }, params).then(result => {
                let isObj = (typeof result.response == "object");
                this.logger.info("WebResponse(enviarBoletoViaSms) %" + (isObj ? "j" : "s"), isObj ? result.response : "Not an object");
                return result;
            }).catch(err => {
                this.logger.error("WebResponse(enviarBoletoViaSms) Error %d", err.code);
                return tokenExceptionHandler.call(this, err, context).then(() => this.enviarBoletoViaSms(context, params))
            });
        }
    }

    criarAcionamento(context, params) {
        if (!this.token) {
            return auth.call(this, context).then(() => this.criarAcionamento(context, params));
        } else {
            this.logger.info("WebRequest(criarAcionamento): %j", params);
            return httpRequest.call(this, context, {
                path: "/v1/criarAcionamento",
                method: "POST"
            }, params).then(result => {
                let isObj = (typeof result.response == "object");
                this.logger.info("WebResponse(criarAcionamento) %" + (isObj ? "j" : "s"), isObj ? result.response : "Not an object");
                return result;
            }).catch(err => {
                this.logger.error("WebResponse(criarAcionamento) Error %d", err.code);
                return tokenExceptionHandler.call(this, err, context).then(() => this.criarAcionamento(context, params))
            });
        }
    }

}
