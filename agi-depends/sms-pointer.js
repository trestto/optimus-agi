
const uuid = require("uuid/v4");

const BaseHttpSms = require("./http").BaseHttpSms;

const _ = require("lodash");

module.exports = class extends BaseHttpSms {
    init(service, config) {
        config.http = _.defaultsDeep(config.http, {
            ssl: true,
            hostname: "sms-api-pointer.pontaltech.com.br",
            path: "/v1/multiple-concat-sms",
            method: "POST",
            headers: {"Content-Type": "application/json"}
        });
        super.init(service, config);
    }

    sendMessage(context, telefone, mensagem) {
        let id = uuid();
        return this.getOptions(context).then(options => this.httpService(context, options, {
            messages: [{
                to: telefone,
                message: mensagem,
                id: id
            }]
        })).then(result => {
            return {id: id, result: result.response.status};
        })
    }
}

