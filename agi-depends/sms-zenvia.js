
const uuid = require("uuid/v4");

const BaseHttpSms = require("./http").BaseHttpSms;

const _ = require("lodash");

module.exports = class extends BaseHttpSms {
    init(service, config) {
        config.http = _.defaultsDeep(config.http, {
            hostname: "api-rest.zenvia360.com.br",
            path: "/services/send-sms",
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        });
        super.init(service, config);
    }

    sendMessage(context, telefone, mensagem) {
        let id = uuid();
        return this.getOptions(context).then(options => {
            return this.service.constructor.getVariable(context, "USERINFO_SMS").then(userinfo => this.httpService(context, options, {
                sendSmsRequest: {
                    to: "55" + telefone,
                    msg: mensagem,
                    callbackOption: "NONE",
                    aggregateId: userinfo,
                    id: id
                }})
            );
        }).then(result => {
            return {id: id, result: result.response.statusCode};
        });
    }
}
