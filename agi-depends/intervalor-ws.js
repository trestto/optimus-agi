
const HTTP_SERVICE = 0;

const http = require("http");
const https = require("https");
const _ = require("lodash");

//----- START: Private Functions -----//

function httpRequest(context, options, postData) {
    _.defaultsDeep(options, this.defaultOptions);
    return this.deps[HTTP_SERVICE].httpRequest(context, options, postData);
}

//----- END: Private Functions -----//

module.exports = class {
    constructor() {
        this.deps = ["http"];
    }

    init(service, config) {
        this.service = service;
        this.httpAgent = config.ssl ? new https.Agent({keepAlive: true}) : new http.Agent({keepAlive: true});
        this.defaultOptions = {
            hostname: config.host,
            port: config.port,
            agent: this.httpAgent,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Basic " + (new Buffer(config.username + ':' + config.password)).toString('base64')
            }
        };
        this.pathPrefix = config.pathPrefix || "";
        this.config = config;
    }

    pesquisaCliente(context, params) {
        this.logger.info("WebRequest(pesquisaCliente): %j", params);
        return httpRequest.call(this, context, {
            path: this.pathPrefix + "/v1/wsPesquisaCliente",
            method: "POST"
        }, params).then(result => {
            this.logger.info("WebResponse(pesquisaCliente): %j", result.response);
            return result;
        });
    }

    agrupamentoDivida(context, params) {
        this.logger.info("WebRequest(agrupamentoDivida): %j", params);
        return httpRequest.call(this, context, {
            path: this.pathPrefix + "/v1/wsAgrupamentoDivida",
            method: "POST"
        }, params).then(result => {
            this.logger.info("WebResponse(agrupamentoDivida): %j", result.response);
            return result;
        });
    }

    condicoesNegociacao(context, params) {
        this.logger.info("WebRequest(condicoesNegociacao): %j", params);
        return httpRequest.call(this, context, {
            path: this.pathPrefix + "/v1/wscondicoesnegociacao",
            method: "POST"
        }, params).then(result => {
            this.logger.info("WebResponse(condicoesNegociacao): %j", result.response);
            return result;
        });
    }

    calculoCondicaoNegociacao(context, params) {
        this.logger.info("WebRequest(calculoCondicaoNegociacao): %j", params);
        return httpRequest.call(this, context, {
            path: this.pathPrefix + "/v1/wscalculocondicaonegociacao",
            method: "POST"
        }, params).then(result => {
            this.logger.info("WebResponse(calculoCondicaoNegociacao): %j", result.response);
            return result;
        });
    }

    emissaoAcordo(context, params) {
        this.logger.info("WebRequest(emissaoAcordo): %j", params);
        return httpRequest.call(this, context, {
            path: this.pathPrefix + "/v1/wsemissaoacordo",
            method: "POST"
        }, params).then(result => {
            this.logger.info("WebResponse(emissaoAcordo): %j", result.response);
            return result;
        });
    }

    segundaViaBoleto(context, params) {
        this.logger.info("WebRequest(segundaViaBoleto): %j", params);
        return httpRequest.call(this, context, {
            path: this.pathPrefix + "/v1/wssegundaviaboleto",
            method: "POST"
        }, params).then(result => {
            this.logger.info("WebResponse(segundaViaBoleto): %j", result.response);
            return result;
        });
    }

    nocaiEnviaSms(context, params) {
        this.logger.info("WebRequest(nocaiEnviaSms): %j", params);
        return httpRequest.call(this, context, {
            path: this.pathPrefix + "/v1/nocaiEnviaSms",
            method: "POST"
        }, params).then(result => {
            this.logger.info("WebResponse(nocaiEnviaSms): %j", result.response);
            return result;
        });
    }
    nocaiSendAction(context, params) {
        this.logger.info("WebRequest(nocaiSendAction): %j", params);
        return httpRequest.call(this, context, {
            path: this.pathPrefix + "/v1/nocaiSendAction",
            method: "POST"
        }, params).then(result => {
            this.logger.info("WebResponse(nocaiSendAction): %j", result.response);
            return result;
        });
    }
}
