
module.exports = class {
    getStreamFile(context) {
        return context.getVariable("AGI_SOUND").then((response) => {
            if (response.code == 200) {
                let m = /^([01]) \((.*)\)/.exec(response.result);
                if (m && parseInt(m[1]))
                    return m[2];
            }
            return null;
        });
    }

    playback(context, filename, async = false) {
        return async ? context.stream.write('STREAM FILE "' + filename + '" ""\n') : context.streamFile(filename);
    }
}
