
const http = require("http");
const https = require("https");
const url = require("url");

const _ = require("lodash");

function parseHostname(options) {
    let hostport = options.hostname;
    if ((options.ssl && options.port != 443) || (!options.ssl && options.port != 80))
        hostport += ":" + options.port;
    return hostport;
}

function logRequest(webRequestId, options, postData) {
    let headers = "";
    for (let header in options.headers)
        headers += header + ": " + options.headers[header] + "\n";
    this.logger.debug(
        "HTTP REQUEST[%d]: %s://%s:%d%s\n" +
        "===== HEADERS START =====\n%s\n===== HEADERS END =====\n" +
        "===== DATA START=====\n%s\n===== DATA END =====",
        webRequestId, options.ssl ? "https" : "http", options.hostname, options.port, options.path,
        headers,
        postData);
}

function logResponse(webRequestId, res, responseData) {
    let headers = "";
    for (let header in res.headers)
        headers += header + ": " + res.headers[header] + "\n";
    this.logger.debug(
        "HTTP RESPONSE[%d]: %d\n" +
        "===== HEADERS START =====\n%s\n===== HEADERS END =====\n" +
        "===== DATA START=====\n%s\n===== DATA END =====",
        webRequestId, res.statusCode,
        headers,
        responseData);
}

class HttpError extends Error {
    constructor(res, data = null) {
        super(res.statusMessage);
        this.code = res.statusCode;
        this.response = res;
        this.data = data;
    }
};

module.exports = class {
    constructor() {
        this.deps = ["agi-sound"];
    }

    init(service, config) {
        this.service = service;
    }

    httpRequest(context, options, postData) {
        var parseGetVariable = function(response) {
            if (response.code == 200) {
                let m = /^([01]) \((.*)\)/.exec(response.result);
                if (m && parseInt(m[1]))
                    return m[2];
            }
            return null;
        };
        if (typeof postData != "string" && typeof postData != "undefined") postData = JSON.stringify(postData);
        options = Object.assign({headers: {}}, options);
        options.headers = Object.assign({"Content-Type": "application/json"}, options.headers);
        if (postData && !options.headers["Content-Length"])
            options.headers["Content-Length"] = Buffer.byteLength(postData);
        if (context === undefined)
            context = null;
        return (context == null ? Promise.resolve(null) : context.getVariable("WEB_SQL_INSERT_LOG_REQUEST_V1(" +
                (options.ssl ? "1" : "0") + "," + // ARG1
                parseHostname(options) + "," + // ARG2
                options.path.replace(",", "\\,") + "," + // ARG3
                options.method + ("Content-Type" in options.headers || postData ? "," + // ARG4
                ("Content-Type" in options.headers ? options.headers["Content-Type"] : "") + (postData ? "," : "") + // ARG5
                encodeURIComponent(postData) : "") + ")" // ARG6
        ).then(parseGetVariable, err => null)).then((reqId) => {
            let bodyResponse = "";
            logRequest.call(this, reqId, options, postData);
            if (context != null) {
                this.deps[0].getStreamFile(context).then(filename => {
                    if (filename != null)
                        this.deps[0].playback(context, filename, true);
                });
            }
            return new Promise((resolve, reject) => {
                const req = (options.ssl ? https : http).request(options, (res) => {
                    var cb = function() {
                        if (res.statusCode >= 200 && res.statusCode <= 299) {
                            var ret = {headers: res.headers};

                            if (/\w+\/json/.test(res.headers["content-type"])) {
                                try {
                                    ret.response = JSON.parse(bodyResponse);
                                } catch (err) {
                                    ret.response = {};
                                }
                            } else {
                                ret.response = bodyResponse;
                            }
                            resolve(ret);
                        } else {
                            reject(new HttpError(res, bodyResponse ? bodyResponse : null));
                        }
                    };
                    var procText = function() {
                        if (/\w+\/json/.test(res.headers["content-type"]) && bodyResponse.length)
                            return JSON.stringify(JSON.parse(bodyResponse));
                        return bodyResponse;
                    }
                    res.setEncoding("utf8");
                    res.on("data", (chunk) => { bodyResponse += chunk; });
                    res.on("end", () => {
                        logResponse.call(this, reqId, res, bodyResponse);
                        if (reqId == null) {
                            cb();
                        } else {
                            let bodyResponseEnc = bodyResponse.length > 500 ? new Array() : encodeURIComponent(procText());
                            if (bodyResponseEnc instanceof Array) {
                                let bodyResponsePart = procText();
                                while (bodyResponsePart.length) {
                                    if (!bodyResponseEnc.length) {
                                        let idx = bodyResponsePart.indexOf(",");
                                        if (idx < 500 && idx >= 0) {
                                            bodyResponseEnc.push(encodeURIComponent(bodyResponsePart.substring(0, idx)));
                                            bodyResponsePart = bodyResponsePart.substring(idx);
                                            continue;
                                        }
                                    }
                                    bodyResponseEnc.push(encodeURIComponent(bodyResponsePart.substring(0, 500)));
                                    bodyResponsePart = bodyResponsePart.substring(500);
                                }
                                context.getVariable("WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(" + bodyResponseEnc.shift() + ")").then(parseGetVariable).then(dataId => {
                                    let getInsertChunkFunc = function(dataChunk, endChunk) {
                                        return function() {
                                            return context.setVariable(!endChunk ?
                                                    "WEB_SQL_INSERT_CHUNK_LOG_RESPONSE_V1(" + dataId + ")" :
                                                    "WEB_SQL_INSERT_END_CHUNK_LOG_RESPONSE_V1(" + dataId + "," + reqId + "," + res.statusCode + "," + res.headers["content-type"] + ")",
                                                            dataChunk);
                                        }
                                    };
                                    let p = Promise.resolve();
                                    while (bodyResponseEnc.length) {
                                        let end = !(bodyResponseEnc.length > 1);
                                        p = p.then(getInsertChunkFunc(bodyResponseEnc.shift(), end));
                                    }
                                    return p;
                                }).then(cb).catch(cb);
                            } else {
                                context.setVariable("WEB_SQL_INSERT_LOG_RESPONSE_V1(" +
                                        reqId + "," +
                                        res.statusCode + "," +
                                        res.headers["content-type"] + ")", bodyResponseEnc).then(cb, cb);
                            }
                        }
                    });
                });
                req.on("error", reject);
                if (typeof postData != "undefined") req.write(postData);
                req.end();
            });
        });
    }
}

module.exports.BaseHttp = class {
    constructor() {
        this.deps = ["http"];
        this.HTTP_SERVICE = 0;
    }

    init(service, config) {
        this.service = service;
        this.config = config;
        this.defaultOptions = {};
        if (this.config instanceof Array) {
            let configGlobal = this.config.find(configItem => configItem.name == "global");
            if (this.config.some(configItem => configItem.ssl))
                this.httpsAgent = new https.Agent({keepAlive: true});
            if (this.config.some(configItem => !configItem.ssl))
                this.httpAgent = new http.Agent({keepAlive: true});
            if (configGlobal)
                this.defaultOptions = this.constructor.getHttpConfig(configGlobal);
            this.defaultOptions.agent = this.defaultOptions.ssl ? this.httpsAgent : this.httpAgent;
        } else {
            this.defaultOptions = this.constructor.getHttpConfig(this.config);
            this.defaultOptions.agent = (this.defaultOptions.ssl ? this.httpsAgent = new https.Agent({keepAlive: true}) : this.httpAgent = new http.Agent({keepAlive: true}));
        }
    }

    get httpService() { return this.deps[this.HTTP_SERVICE]; }

    static getConfig(context, config, varname = "CONFIG_HTTP") {
        const varRE = /^([01]) \((.*)\)/;
        if (!Array.isArray(config))
            return Promise.resolve(config);
        let configGlobal = config.find(configItem => configItem.name == "global");
        return context.getVariable(varname).then(function(response) {
            let m = varRE.exec(response.result);
            if (!m)
                return configGlobal;
            let configItem = config.find(configItem => configItem.name == m[2]);
            return Object.assign({}, configGlobal, typeof configItem == "object" ? configItem : {});
        });
    }

    static getHttpConfig(configItem) {
        let config = {};
        for (let kw in configItem) {
            switch (kw) {
                case "host":
                    config.hostname = configItem[kw];
                    break;

                case "port":
                    if (configItem[kw] != (configItem.ssl ? 443 : 80))
                        config.port = configItem[kw];
                    break;

                case "ssl":
                case "path":
                    config[kw] = configItem[kw];
                    break;
            }
        }
        return config;
    }
}

module.exports.BaseHttpSms = class {
    constructor() {
        this.deps = ["http"];
        this.HTTP_SERVICE = 0;
    }

    init(service, config) {
        this.service = service;
        this.httpsAgent = new https.Agent({keepAlive: true});
        this.httpAgent = new http.Agent({keepAlive: true});
        this.defaultOptions = Object.assign({}, config.http);
        this.defaultOptions.agent = this.defaultOptions.ssl ? this.httpsAgent : this.httpAgent;
    }

    httpService() { return this.deps[this.HTTP_SERVICE].httpRequest.apply(this.deps[this.HTTP_SERVICE], arguments); }

    getOptions(context) {
        return this.service.constructor.getVariable(context, "URL_SMS").then(urlsms => {
            return urlsms ? url.parse(urlsms) : {};
        }).then(urlsms => {
            if (urlsms.protocol) {
                if (urlsms.protocol == "https:")
                    urlsms.ssl = true;
                urlsms.agent = urlsms.ssl ? this.httpsAgent : this.httpAgent;
            }
            return this.service.constructor.getVariable(context, "USERNAME_SMS").then(username => [username])
                                                                                .then(auth => this.service.constructor.getVariable(context, "PASSWORD_SMS").then(password => auth.concat([password])))
                                                                                .then(auth => {
                if (auth.every(item => item))
                    urlsms.auth = auth.join(":");
                else if (auth[0])
                    urlsms.auth = auth[0];
                return urlsms;
            });
        }).then(urlsms => _.defaults(_.pickBy(_.pick(urlsms, ["ssl", "agent", "auth", "hostname", "port", "path"]), (value, key) => value != null), this.defaultOptions));
    }
}

module.exports.HttpError = HttpError;
