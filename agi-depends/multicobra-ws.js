const HTTP_SERVICE = 0;

module.exports = class {
  constructor() {
    this.deps = ['http'];
  }

  init(service, config) {
    this.service = service;
    this.config = config;
  }


  validarDevedor(context, params) {
    const options = {
      path: '/v1/validarDevedor',
      method: 'POST',
    };

    const data = {
      Identificador: params.cpf,
    };

    return this._httpRequest(context, options, data)
      .then(result => result.response);
  }

  consultarDividasAtivas(context, params) {
    const options = {
      path: '/v1/consultarDividasAtivas',
      method: 'POST',
    };

    const data = {
      Identificador: params.cpf,
    };

    return this._httpRequest(context, options, data)
      .then(result => result.response);
  }

  consultarParcelasNegociaveis(context, params) {
    const options = {
      path: '/v1/consultarParcelasNegociaveis',
      method: 'POST',
    };

    let contratos;

    if(params.contrato.indexOf(',') !== -1) {
      contratos = params.contrato.split(',');
    }
    else {
      contratos = [params.contrato];
    }

    const data = {
      Integracao: params.integracao,
      DevedorCodigo: params.devedorCodigo,
      Filial: params.filial,
      Vencimento: params.vencimento,
      Contratos: contratos,
    };

    return this._httpRequest(context, options, data)
      .then(result => result.response);
  }

  listarParcelamentos(context, params) {
    const options = {
      path: '/v1/listarParcelamentos',
      method: 'POST',
    };

    let contratoParcelaArr;

    if(params.contratoParcela.indexOf(',') !== -1) {
      contratoParcelaArr = params.contratoParcela.split(',');
    }
    else {
      contratoParcelaArr = [params.contratoParcela];
    }

    const contratosParcelas = contratoParcelaArr.map(item => {
      const parts = item.split('-');
      const contrato = parts[0];
      const parcela = parts[1];

      return {
        ContratoCodigo: contrato,
        ParcelaCodigo: parcela,
      };
    });

    const parceladoBoolean = Boolean(parseInt(params.parcelado));

    const data = {
      ChaveControleNegociacao: params.chaveControleNegociacao,
      Data: params.vencimento,
      Entrada: params.entrada,
      Parcelado: parceladoBoolean,
      ContratosParcelas: contratosParcelas,
    };

    return this._httpRequest(context, options, data)
      .then(result => result.response);
  }

  salvarNegociacao(context, params) {
    const options = {
      path: '/v1/salvarNegociacao',
      method: 'POST',
    };

    const data = {
      ChaveControleNegociacao: params.chaveControleNegociacao,
      CodigoParcelamento: params.codigoParcelamento,
    };

    if(params.email) {
      data['Email'] = params.email;
    }

    if(params.ddd && params.telefone) {
      data['DDD'] = params.ddd;
      data['Telefone'] = params.telefone;
    }

    return this._httpRequest(context, options, data)
      .then(result => result.response);
  }


  _httpRequest(context, options, postData) {
    const config = this.config;

    const defaultOptions = {
      hostname: config.host,
      port: config.port,
      agent: new require((config.ssl ? 'https' : 'http')).Agent({keepAlive: true}),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic " + (new Buffer(config.username + ':' + config.password)).toString('base64')
      },
    };

    const optionsParam = Object.assign({headers: {}}, defaultOptions, options);

    return this.deps[HTTP_SERVICE]
      .httpRequest(context, optionsParam, postData);
  }
};
