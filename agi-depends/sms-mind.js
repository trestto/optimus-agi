
const BaseHttpSms = require("./http").BaseHttpSms;

const _ = require("lodash");

module.exports = class extends BaseHttpSms {
    init(service, config) {
        config.http = _.defaultsDeep(config.http, {
            hostname: "189.108.159.122",
            port: 2738,
            path: "/sms/send",
            method: "GET"
        });
        super.init(service, config);
    }

    sendMessage(context, telefone, mensagem) {
        return this.getOptions(context).then(options => {
            var data = new Array();
            if (options.auth) {
                data.push(["u", encodeURIComponent(options.auth)].join("="));
                delete options.auth;
            } else {
                throw new Error("Variavel de autenticacao USERNAME_SMS nao setada");
            }
            data.push(["f", 2].join("="));
            data.push(["n", encodeURIComponent(telefone)].join("="));
            data.push(["m", encodeURIComponent(mensagem)].join("="));
            options.path += "?" + data.join("&");
            return this.httpService(context, options);
        }).then(result => {
            let response = result.response.split(";");
            return {id: response[0] == 1 ? response[1] : null, result: response[0]};
        });
    }
}
