
const HTTP_SERVICE = 0;

const http = require("http");
const https = require("https");

const BaseHttp = require("./http").BaseHttp;

const BaseHttpSms = require("./http").BaseHttpSms;

const _ = require("lodash");

module.exports = class extends BaseHttpSms {
    init(service, config) {
        config.http = _.defaultsDeep(config.http, {
            hostname: "179.127.4.110",
            path: "/services_api/envio_sms",
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            }
        });
        super.init(service, config);
    }

    sendMessage(context, telefone, mensagem) {
        return this.getOptions(context).then(options => {
            let username, password;
            if (options.auth) {
                let pos = options.auth.indexOf(":");
                
                if (pos < 0)
                    throw new Error("Variavel PASSWORD_SMS nao setada");
                username = options.auth.substring(0, pos);
                password = options.auth.substring(pos + 1);
                delete options.auth;
            } else {
                throw new Error("Variavel de autenticacao USERNAME_SMS nao setada");
            }
            return this.httpService(context, options, [
                ["usuario", encodeURIComponent(username)].join("="),
                ["senha", encodeURIComponent(password)].join("="),
                ["sms", encodeURIComponent(mensagem)].join("="),
                ["destinatario", encodeURIComponent(telefone)].join("=")
            ].join("&"));
        }).then(result => {
            return {id: result.response.data, result: result.response.id, message: result.response.msg};
        });
    }
}
