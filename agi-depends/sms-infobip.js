
const http = require("http");
const https = require("https");

const BaseHttpSms = require("./http").BaseHttpSms;

const _ = require("lodash");

module.exports = class extends BaseHttpSms {
    init(service, config) {
        config.http = _.defaultsDeep(config.http, {
            method: "POST",
            path: "/sms/1/text/single"
        });
        super.init(service, config);
    }

    sendMessage(context, telefone, mensagem) {
        return this.getOptions(context).then(options => this.httpService(context, options, {
            from: "InfoSMS",
            to: "55" + telefone,
            text: mensagem
        })).then(result => result.response.messages instanceof Array && result.response.messages.length ?
                           {id: result.response.messages[0].messageId, result: result.response.messages[0].status.name} :
                           {id: "", result: ""});
    }
}
