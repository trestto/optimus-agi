
const soap = require("soap");
const uuid = require("uuid/v4");

module.exports = class {
    init(service, config) {
        this.service = service;
        this.config = config;
        this.soapClient = {};
    }

    sendMessage(context, telefone, mensagem) {
        return this.service.constructor.getVariable(context, "URL_SMS").then(url => url ? url : this.config.url).then(url => {
            if (!url)
                throw new Error("Variavel URL_SMS nao setada");
            if (url in this.soapClient)
                return this.soapClient[url];
            return new Promise((resolve, reject) => {
                soap.createClient(url, (err, client) => {
                    if (err) {
                        reject(err);
                    } else {
                        this.soapClient[url] = client;
                        resolve(this.soapClient[url]);
                    }
                });
            });
        }).then(soapClient => {
            return this.service.constructor.getVariable(context, "USERNAME_SMS").then(username => {
                if (!username)
                    throw new Error("Variavel USERNAME_SMS nao setada");
                return {usuario: username};
            }).then(args => {
                return this.service.constructor.getVariable(context, "PASSWORD_SMS").then(password => {
                    if (!password)
                        throw new Error("Variavel PASSWORD_SMS nao setada");
                    args.senha = password;
                    return args;
                });
            }).then(args => {
                args.id = uuid();
                args.telefone = telefone;
                args.mensagem = mensagem;
                return new Promise((resolve, reject) => {
                    soapClient.EnviaMensagem(args, (err, result) => {
                        if (err) 
                            reject(err);
                        else
                            resolve({id: args.id, result: parseInt(result.ComObsResult)});
                    });
                });
            });
        });
    }
}
