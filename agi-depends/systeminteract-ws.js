const HTTP_SERVICE = 0;

module.exports = class {
  constructor() {
    this.deps = ['http'];
  }

  init(service, config) {
    this.service = service;
    this.config = config;
  }

  SetMailingRetorno(context, params) {
    const options = {
      path: '/v1/setMailingRetorno',
      method: 'POST',
    };

    const config = this.config;

    const data = Object.assign({ ChaveAcesso: config.chaveAcesso }, params);

    return this._httpRequest(context, options, data)
      .then(result => result.response);
  }


  _httpRequest(context, options, postData) {
    const config = this.config;

    const defaultOptions = {
      hostname: config.host,
      port: config.port,
      agent: new require((config.ssl ? 'https' : 'http')).Agent({keepAlive: true}),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const optionsParam = Object.assign({headers: {}}, defaultOptions, options);

    return this.deps[HTTP_SERVICE]
      .httpRequest(context, optionsParam, postData);
  }
};
