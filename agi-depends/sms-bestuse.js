
const BaseHttpSms = require("./http").BaseHttpSms;

const _ = require("lodash");

module.exports = class extends BaseHttpSms {
    init(service, config) {
        config.http = _.defaultsDeep(config.http, {
            hostname: "v2.bestuse.com.br",
            path: "/api/v1/envioApi",
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            }
        });
        super.init(service, config);
    }

    sendMessage(context, telefone, mensagem) {
        return this.getOptions(context).then(options => {
            if (options.auth) {
                options.path += "?token=" + encodeURIComponent(options.auth);
                delete options.auth;
            } else {
                throw new Error("Variavel de autenticacao USERNAME_SMS nao setada");
            }
            return this.service.constructor.getVariable(context, "USERINFO_SMS").then(userinfo => this.httpService(context, options, {
                smss: [{
                    numero: telefone,
                    mensagem: mensagem
                }],
                envioImediato: true,
                centroCusto: userinfo
            }));
        }).then(result => {
            return {id: "", result: result.response.success};
        });
    }
}